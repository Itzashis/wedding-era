import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
@Injectable({ providedIn: 'root' })
export class GalleryService {
  private headers = new HttpHeaders().set('Content-Type', 'application/json');
  private options = { headers: this.headers, withCredentials: true };
  allImages: any = [];
  constructor(private http: HttpClient) {
  }


  getShortWidgetData(): any {
    return this.http.get(environment.appUrl + 'gallery/getAllImage')
  }


  getHeadingData(): any {
    return this.http.get(environment.appUrl + 'home/getOtherContent')
  }


  private handleError(error: Response): any {
    return Observable.throw(error.statusText);
  }
}


