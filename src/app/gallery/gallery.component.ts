import { Component, OnInit, OnChanges } from '@angular/core';
import { GalleryService } from './gallery.service';
import { NgxGalleryOptions } from '@kolkov/ngx-gallery';
import { NgxGalleryImage } from '@kolkov/ngx-gallery';
import { forkJoin, Subscription } from 'rxjs';
import { Lightbox } from 'ngx-lightbox';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss'],
})
export class GalleryComponent implements OnInit {
  loadingModal = true;

  gallery_container: any;
  galleryOptions: NgxGalleryOptions[];
  galleryImages: NgxGalleryImage[];
  isImageBoxVisible = false;
  heading_container: any;
  allImages: any[] = [];


  constructor(
    private auth: GalleryService,
    private _lightbox: Lightbox,
    private activatedRoute: ActivatedRoute,
    private route: Router,
  ) {
    // this.allImages = this.auth.getImages();
  }

  ngOnInit(): void {
    this.gallery_container = []
    let allHeading = this.auth.getHeadingData();
    let galleryImage = this.auth.getShortWidgetData();


    forkJoin([allHeading, galleryImage]).subscribe(data => {
      this.loadAllHadeing(data[0]);
      this.loadgalleryImage(data[1]);
      this.loadingModal = false;
    }, err => {

    })
  }

  open(index: number): void {
    // open lightbox
    this._lightbox.open(this.allImages, index);
  }

  close(): void {
    // close lightbox programmatically
    this._lightbox.close();
  }



  loadAllHadeing(data: any) {
    this.heading_container = {};
    this.heading_container = data['Payload'][0]
  }

  loadgalleryImage(data: any) {
    this.gallery_container = []
    this.auth.getShortWidgetData().subscribe((data: any) => {
      let formatted_gallery_data_map: any = {};

      data['Payload'].forEach((statusMap: any, indx: any) => {

        let gallery_display_name = statusMap['galleryName'];

        let gallery_data_array: any = [];
        if (!formatted_gallery_data_map.hasOwnProperty(gallery_display_name)) {
          gallery_data_array = [];
          formatted_gallery_data_map[gallery_display_name] = gallery_data_array;
        }
        gallery_data_array = formatted_gallery_data_map[gallery_display_name];
        let node = {
          'galleryId': statusMap['galleryId'],
          'imageName': statusMap['imageName'],
          'imageUrl': statusMap['imageUrl'],
          'imageBasePath': statusMap['imageBasePath'],
          'imageId': statusMap['imageId'],
        };
        gallery_data_array.push(node);
      });

      for (const [name, data_array] of Object.entries(formatted_gallery_data_map)) {
        this.gallery_container.push({ 'galleryName': name, 'data': data_array });
      }
    })
  }

  showGallery(galleryId: any) {
    debugger
    let navigationExtras: NavigationExtras = {
      queryParams: {
        'galleryId': galleryId,
      }
    };

    this.route.navigate(['/albums/'], navigationExtras);
  }


}


// "@ngx-gallery/core": "^4.0.3",
//   "@ngx-gallery/lightbox": "^4.0.3",
