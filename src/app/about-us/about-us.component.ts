import { Component, OnInit } from '@angular/core';
import { AboutUsService } from './aboutus.service';

@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.scss']
})
export class AboutUsComponent implements OnInit {
  about_container: any;
  loadingModal = true;
  constructor(
    private auth: AboutUsService
  ) { }

  ngOnInit(): void {
    this.about_container = {};
    this.auth.getAboutUsData().subscribe((data: any) => {
      this.about_container = data['Payload'][0];
      this.loadingModal = false;
    })
  }

}
