import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
@Injectable({ providedIn: 'root' })
export class AboutUsService {
  private headers = new HttpHeaders().set('Content-Type', 'application/json');
  private options = { headers: this.headers, withCredentials: true };

  constructor(private http: HttpClient) {
  }

  getAboutUsData(): any {
    return this.http.get(environment.appUrl + 'home/getAboutUs')
  }



  private handleError(error: Response): any {
    return Observable.throw(error.statusText);
  }

}
