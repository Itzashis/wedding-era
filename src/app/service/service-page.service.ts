import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
@Injectable({ providedIn: 'root' })
export class ServicePageService {
  private headers = new HttpHeaders().set('Content-Type', 'application/json');
  private options = { headers: this.headers, withCredentials: true };

  constructor(private http: HttpClient) {
  }

  getServiceData(): any {
    return this.http.get(environment.appUrl + 'service/getServiceDetails')
  }
  getHeadingData(): any {
    return this.http.get(environment.appUrl + 'home/getOtherContent')
  }

  private handleError(error: Response): any {
    return Observable.throw(error.statusText);
  }

}
