import { Component, OnInit } from '@angular/core';
import { ServicePageService } from './service-page.service';
import { forkJoin, Subscription } from 'rxjs';

@Component({
  selector: 'app-service',
  templateUrl: './service.component.html',
  styleUrls: ['./service.component.scss']
})
export class ServiceComponent implements OnInit {
  service_container: any;
  heading_container: any;
  loadingModal = true;
  constructor(
    private auth: ServicePageService
  ) { }

  ngOnInit(): void {

    let allHeading = this.auth.getHeadingData();
    let serviceData = this.auth.getServiceData();


    forkJoin([allHeading, serviceData]).subscribe(data => {
      this.loadAllHadeing(data[0]);
      this.leadServiceData(data[1]);
      this.loadingModal = false;
    }, err => {

    })
  }

  loadAllHadeing(data: any) {
    this.heading_container = {};
    this.heading_container = data['Payload'][0]
  }

  leadServiceData(data: any) {
    this.service_container = [];
    data['Payload'].forEach((element: any, idx: any) => {
      if (idx == 0) {
        element['isActive'] = true
      } else {
        element['isActive'] = false
      }
      element['tabId'] = "tab-" + idx
      this.service_container.push(element)
    });

    console.log(this.service_container)
  }

}
