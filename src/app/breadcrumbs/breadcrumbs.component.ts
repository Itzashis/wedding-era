import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-breadcrumbs',
  templateUrl: './breadcrumbs.component.html',
  styleUrls: ['./breadcrumbs.component.scss']
})
export class BreadcrumbsComponent implements OnInit {

  pageTitleBreaCrumb: any;


  @Input('pageTitle')
  set pageTitle(value: any) {
    this.pageTitleBreaCrumb = value;
  }

  constructor() { }

  ngOnInit(): void {
  }

}
