import { Component, OnInit, HostListener } from '@angular/core';
import { HeaderService } from './navigation.service';
@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {
  header_container: any;
  isHeaderInfo = true;
  constructor(
    private auth: HeaderService
  ) {

  }

  ngOnInit(): void {
    this.header_container = {};
    this.auth.getHeaderData().subscribe((data: any) => {
      this.header_container = data['Payload'][0]
    })
  }


  @HostListener("window:scroll", []) onWindowScroll() {
    // do some stuff here when the window is scrolled
    const verticalOffset = window.pageYOffset
      || document.documentElement.scrollTop
      || document.body.scrollTop || 0;
    if (verticalOffset > 200) {
      this.isHeaderInfo = false;
    } else {
      this.isHeaderInfo = true;
    }
  }



}
