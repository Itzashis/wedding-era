import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
@Injectable({ providedIn: 'root' })
export class HeaderService {
  private headers = new HttpHeaders().set('Content-Type', 'application/json');
  private options = { headers: this.headers, withCredentials: true };

  constructor(private http: HttpClient) {
  }

  getHeaderData(): any {
    return this.http.get(environment.appUrl + 'headerfooter/getHeaderFooterData')
  }

  private handleError(error: Response): any {
    return Observable.throw(error.statusText);
  }

}
