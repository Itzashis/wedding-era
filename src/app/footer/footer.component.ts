import { Component, OnInit } from '@angular/core';
import { FooterService } from './footer.service';
import { forkJoin, Subscription } from 'rxjs';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  footer_container: any;
  contact_container: any;
  isFooter = false
  public title = "THE PRESET MESSAGE TO SEND";

  constructor(
    private auth: FooterService
  ) { }

  ngOnInit(): void {
    let allfooter = this.auth.getHeaderData();
    let contactDate = this.auth.getContactData();


    forkJoin([allfooter, contactDate]).subscribe(data => {
      this.loadAllFooter(data[0]);
      this.loadContactData(data[1]);
      this.isFooter = true
    }, err => {

    })

  }

  loadAllFooter(data: any) {
    this.footer_container = {}
    this.footer_container = data['Payload'][0]
  }

  loadContactData(data: any) {
    this.contact_container = {};
    this.contact_container = data['Payload'][0]
  }

}
