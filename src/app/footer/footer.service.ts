import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
@Injectable({ providedIn: 'root' })
export class FooterService {
  private headers = new HttpHeaders().set('Content-Type', 'application/json');
  private options = { headers: this.headers, withCredentials: true };

  constructor(private http: HttpClient) {
  }

  getHeaderData(): any {
    return this.http.get(environment.appUrl + 'headerfooter/getHeaderFooterData')
  }


  getContactData(): any {
    return this.http.get(environment.appUrl + 'contact/getContactData')
  }

  private handleError(error: Response): any {
    return Observable.throw(error.statusText);
  }

}
