import { Component, OnInit } from '@angular/core';
import { GalleryService } from '../gallery/gallery.service';
import { ActivatedRoute } from '@angular/router';
import { Lightbox } from 'ngx-lightbox';

@Component({
  selector: 'app-image-detail',
  templateUrl: './image-detail.component.html',
  styleUrls: ['./image-detail.component.scss']
})
export class ImageDetailComponent implements OnInit {

  images: any
  galleryName: any;

  constructor(private auth: GalleryService,
    private activatedRoute: ActivatedRoute, private _lightbox: Lightbox) { }

  ngOnInit() {

    this.images = [];
    let galleryId = this.activatedRoute.snapshot.queryParams['galleryId']
    this.auth.getShortWidgetData().subscribe((data: any) => {
      data['Payload'].forEach((element: any) => {
        if (element.galleryId === galleryId) {
          element['src'] = element.imageUrl
          this.images.push(element)
        }
      });
    })
    this.galleryName = this.images[0]['galleryName']


  }
  open(index: number): void {
    // open lightbox
    this._lightbox.open(this.images, index);
  }

  close(): void {
    // close lightbox programmatically
    this._lightbox.close();
  }

}
