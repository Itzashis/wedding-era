import { Component, OnInit } from '@angular/core';
import { Lightbox } from 'ngx-lightbox';
import { VideoGalleryService } from './video-gallery.service';

@Component({
  selector: 'app-video-gallery',
  templateUrl: './video-gallery.component.html',
  styleUrls: ['./video-gallery.component.scss']
})
export class VideoGalleryComponent implements OnInit {
  loadingModal = true;
  imageObject: any
  videoGallery: any;
  imageSize = {
    width: '300px',
    height: '200px',
  }
  constructor(private _lightbox: Lightbox, private auth: VideoGalleryService) { }

  ngOnInit(): void {
    this.videoGallery = [];
    this.auth.getAllVideos().subscribe((data: any) => {
      data['Payload'].forEach((element: any) => {
        let obj = {};
        obj = {
          gallery: [{
            video: element.videoLink
          }],
          videotext: element.videoName
        }
        this.videoGallery.push(obj)
      });
      this.loadingModal = false;
    })

  }
  open(index: number): void {
    // open lightbox
    this._lightbox.open(this.videoGallery, index);
  }

  close(): void {
    // close lightbox programmatically
    this._lightbox.close();
  }
}
