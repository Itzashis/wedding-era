import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
@Injectable({ providedIn: 'root' })
export class ContactService {
  private headers = new HttpHeaders().set('Content-Type', 'application/json');
  private options = { headers: this.headers, withCredentials: true };

  constructor(private http: HttpClient) {
  }

  getContactData(): any {
    return this.http.get(environment.appUrl + 'contact/getContactData')
  }

  getHeadingData(): any {
    return this.http.get(environment.appUrl + 'home/getOtherContent')
  }

  sendUserData(body: any) {
    return this.http
      .post(environment.appUrl + 'user/addNewUser', JSON.stringify(body), this.options)
  }

  private handleError(error: Response): any {
    return Observable.throw(error.statusText);
  }

}
