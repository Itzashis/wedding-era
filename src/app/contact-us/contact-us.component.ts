import { Component, OnInit } from '@angular/core';
import { forkJoin, Subscription } from 'rxjs';
import { ContactService } from './contact.service';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.scss']
})
export class ContactUsComponent implements OnInit {
  heading_container: any;
  loadingModal = true;
  contact_container: any;
  contact_map_src: any;
  user_fullName: any;
  user_email: any;
  user_subject: any;
  user_mobile_number: any;
  user_msg: any;
  isSuccess = false
  isNotSuccess = false

  constructor(
    private auth: ContactService
  ) { }

  ngOnInit(): void {
    let allHeading = this.auth.getHeadingData();
    let contactDate = this.auth.getContactData();


    forkJoin([allHeading, contactDate]).subscribe(data => {
      this.loadAllHadeing(data[0]);
      this.loadContactData(data[1]);
      this.loadingModal = false;
    }, err => {

    })
  }

  loadAllHadeing(data: any) {
    this.heading_container = {};
    this.heading_container = data['Payload'][0]
  }


  loadContactData(data: any) {
    this.contact_container = {};
    this.contact_container = data['Payload'][0]

    let mapApi = this.contact_container.contactMap.split('=')
    let mapSrc = mapApi[2].split('"')

    this.contact_map_src = '"' + "https://www.google.com/maps/embed?pb=" + mapSrc[0] + '"'

    console.log(this.contact_map_src)
  }


  submitUserFrom() {
    let obj = {
      userFullName: this.user_fullName,
      userEmail: this.user_email,
      userSubject: this.user_subject,
      userMob: this.user_mobile_number,
      userMsg: this.user_msg
    }

    this.auth.sendUserData(obj).subscribe((data: any) => {
      if (data['code'] == 200) {
        this.isNotSuccess = false;
        this.isSuccess = true;
        this.emptyFrom();
      } else {
        this.isNotSuccess = true;
        this.isSuccess = false
      }
    })
  }

  emptyFrom() {
    this.user_fullName = '';
    this.user_email = '';
    this.user_subject = '';
    this.user_mobile_number = '';
    this.user_msg = '';
  }

}
