import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { GalleryComponent } from './gallery/gallery.component';
import { ServiceComponent } from './service/service.component';
import { RouterModule, Routes } from '@angular/router';
import { NavigationComponent } from './navigation/navigation.component';
import { FooterComponent } from './footer/footer.component';
import { BreadcrumbsComponent } from './breadcrumbs/breadcrumbs.component';
import { NgxGalleryModule } from '@kolkov/ngx-gallery';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { FormsModule } from '@angular/forms';
import { NgxWhastappButtonModule } from 'ngx-whatsapp-button';
import { ImageDetailComponent } from './image-detail/image-detail.component';
import { FilterimagesPipe } from "./filterimages.pipe";
import { LightboxModule } from 'ngx-lightbox';
import { VideoGalleryComponent } from './video-gallery/video-gallery.component';
import { NgImageSliderModule } from 'ng-image-slider';
import { LoaderComponent } from './loader/loader.component';
const routes = [
  {
    path: '',
    component: HomeComponent,
  },
  {
    path: 'home',
    component: HomeComponent,
  },
  {
    path: 'about-us',
    component: AboutUsComponent,
  },
  {
    path: 'contsct-us',
    component: ContactUsComponent,
  },
  {
    path: 'gallery',
    component: GalleryComponent,
  },
  {
    path: 'service',
    component: ServiceComponent,
  },
  {
    path: 'albums',
    component: ImageDetailComponent,
  },
  {
    path: 'video-gallery',
    component: VideoGalleryComponent,
  }
];

@NgModule({
  declarations: [

    AppComponent,
    HomeComponent,
    AboutUsComponent,
    ContactUsComponent,
    GalleryComponent,
    ServiceComponent,
    NavigationComponent,
    FooterComponent,
    BreadcrumbsComponent,
    ImageDetailComponent,
    FilterimagesPipe,
    VideoGalleryComponent,
    LoaderComponent




  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgxGalleryModule,
    RouterModule.forChild(routes),
    HttpClientModule,
    CarouselModule,
    FormsModule,
    NgxWhastappButtonModule,
    BrowserAnimationsModule,
    LightboxModule,
    NgImageSliderModule
  ],
  exports: [NavigationComponent, FooterComponent, BreadcrumbsComponent, LoaderComponent],
  providers: [FilterimagesPipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
