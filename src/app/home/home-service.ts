import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
@Injectable({ providedIn: 'root' })
export class HomeService {
  private headers = new HttpHeaders().set('Content-Type', 'application/json');
  private options = { headers: this.headers, withCredentials: true };

  constructor(private http: HttpClient) {
  }

  getHeadingData(): any {
    return this.http.get(environment.appUrl + 'home/getOtherContent')
  }
  getBannerImages(): any {
    return this.http.get(environment.appUrl + 'home/getBannerImages')
  }
  getShortWidgetData(): any {
    return this.http.get(environment.appUrl + 'home/getShortWidget')
  }
  getAboutUsData(): any {
    return this.http.get(environment.appUrl + 'home/getAboutUs')
  }
  getServiceData(): any {
    return this.http.get(environment.appUrl + 'service/getServiceDetails')
  }
  getTestimonialDetails(): any {
    return this.http.get(environment.appUrl + 'testimonial/getTestimonialDetails')
  }
  getTeamData(): any {
    return this.http.get(environment.appUrl + 'member/getMemberData')
  }
  getContactData(): any {
    return this.http.get(environment.appUrl + 'contact/getContactData')
  }
  getGalleryImage(): any {
    return this.http.get(environment.appUrl + 'gallery/getAllImage')
  }


  sendUserData(body: any) {
    return this.http
      .post(environment.appUrl + 'user/addNewUser', JSON.stringify(body), this.options)
  }

  private handleError(error: Response): any {
    return Observable.throw(error.statusText);
  }

}
