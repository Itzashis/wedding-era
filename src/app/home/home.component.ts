import { Component, OnInit, HostListener } from '@angular/core';
import { HomeService } from './home-service';
import { forkJoin, Subscription } from 'rxjs';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { NgwWowService } from 'ngx-wow';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {

  heading_container: any;
  short_widget_container: any;
  about_container: any;
  service_container: any
  backgroundImage: any;
  team_container: any;
  contact_container: any;
  contact_map_src: any;
  slidesStore: any;
  testimonialContainer: any;
  gallery_container: any;

  isImageBoxVisible = false;
  user_fullName: any;
  user_email: any;
  user_subject: any;
  user_mobile_number: any;
  user_msg: any;
  isSuccess = false
  isNotSuccess = false;

  loadingModal = false;

  customOptions: OwlOptions = {
    loop: true,
    mouseDrag: false,
    touchDrag: false,
    pullDrag: false,
    dots: false,
    navSpeed: 700,
    autoWidth: true,
    autoplaySpeed: 3000,
    autoplay: true,
    animateIn: 'fadeIn',
    animateOut: 'fadeOut',
    nav: true,
    navText: ['<i class="fa fa-chevron-left"></i>', '<i class="fa fa-chevron-right></i>"'],
    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 1
      },
      740: {
        items: 1
      },
      940: {
        items: 1
      }
    },
  }

  testimonialOption: OwlOptions = {
    loop: true,
    mouseDrag: false,
    touchDrag: false,
    pullDrag: false,
    dots: false,
    navSpeed: 300,
    autoWidth: true,
    autoplaySpeed: 3000,
    autoplay: true,
    nav: true,
    navText: ['<i class="fa fa-chevron-left"></i>', '<i class="fa fa-chevron-right></i>"'],
    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 1
      },
      740: {
        items: 1
      },
      940: {
        items: 2
      }
    },
  }



  constructor(
    private auth: HomeService,
    private activatedRoute: ActivatedRoute,
    private route: Router,
    private wowService: NgwWowService
  ) {
  }

  ngOnInit(): void {
    this.wowService.init();
    let allHeading = this.auth.getHeadingData();
    let allBanner = this.auth.getBannerImages();
    let shortEidget = this.auth.getShortWidgetData();
    let aboutData = this.auth.getAboutUsData();
    let serviceData = this.auth.getServiceData();
    let teamData = this.auth.getTeamData();
    let contactData = this.auth.getContactData();
    let testiContent = this.auth.getTestimonialDetails();
    let galleryImage = this.auth.getGalleryImage();

    forkJoin([allHeading, shortEidget, aboutData, serviceData, teamData, contactData, allBanner, testiContent, galleryImage]).subscribe(data => {

      this.loadAllHadeing(data[0]);
      this.loadShortWidget(data[1]);
      this.loadAboutUsData(data[2]);
      this.leadServiceData(data[3]);
      this.leadTeamMember(data[4]);
      this.loadContactData(data[5]);
      this.loadAllBanner(data[6]);
      this.loadTestMonialDetaills(data[7]);
      this.loadgalleryImage(data[8]);

      this.loadingModal = false;

    }, err => {

    })
  }

  loadAllHadeing(data: any) {
    this.heading_container = {};
    this.heading_container = data['Payload'][0]
  }

  loadShortWidget(data: any) {
    this.short_widget_container = [];
    this.short_widget_container = data['Payload']
  }


  loadAboutUsData(data: any) {
    this.about_container = {};
    this.about_container = data['Payload'][0]
    this.backgroundImage = 'uri(' + this.about_container.aboutImageUrl + ')'
  }

  leadServiceData(data: any) {
    this.service_container = [];
    data['Payload'].forEach((element: any, idx: any) => {
      if (idx == 0) {
        element['isActive'] = true
      } else {
        element['isActive'] = false
      }
      element['tabId'] = "tab-" + idx
      this.service_container.push(element)
    });

    console.log(this.service_container)
  }

  leadTeamMember(data: any) {
    this.team_container = [];
    this.team_container = data['Payload']
  }

  loadContactData(data: any) {
    this.contact_container = {};
    this.contact_container = data['Payload'][0]

    let mapApi = this.contact_container.contactMap.split('=')
    let mapSrc = mapApi[2].split('"')

    this.contact_map_src = '"' + "https://www.google.com/maps/embed?pb=" + mapSrc[0] + '"'

    console.log(this.contact_map_src)
  }

  loadAllBanner(data: any) {
    this.slidesStore = [];
    this.slidesStore = data['Payload'];
  }

  loadTestMonialDetaills(data: any) {
    this.testimonialContainer = []
    this.testimonialContainer = data['Payload']
  }

  loadgalleryImage(data: any) {
    let formatted_gallery_data_map: any = {};
    this.gallery_container = [];
    data['Payload'].forEach((statusMap: any, indx: any) => {

      let gallery_display_name = statusMap['galleryName'];

      let gallery_data_array: any = [];
      if (!formatted_gallery_data_map.hasOwnProperty(gallery_display_name)) {
        gallery_data_array = [];
        formatted_gallery_data_map[gallery_display_name] = gallery_data_array;
      }
      gallery_data_array = formatted_gallery_data_map[gallery_display_name];
      let node = {
        'galleryId': statusMap['galleryId'],
        'imageName': statusMap['imageName'],
        'imageUrl': statusMap['imageUrl'],
        'imageBasePath': statusMap['imageBasePath'],
        'imageId': statusMap['imageId'],
      };
      gallery_data_array.push(node);

    });

    for (const [name, data_array] of Object.entries(formatted_gallery_data_map)) {
      this.gallery_container.push({ 'galleryName': name, 'data': data_array });
    }
    // this.gallery_container = Array(5).fill(0).map((x, i) => i);
  }
  showGallery(galleryId: any) {
    debugger
    let navigationExtras: NavigationExtras = {
      queryParams: {
        'galleryId': galleryId,
      }
    };

    this.route.navigate(['/albums/'], navigationExtras);
  }



  submitUserFrom() {
    let obj = {
      userFullName: this.user_fullName,
      userEmail: this.user_email,
      userSubject: this.user_subject,
      userMob: this.user_mobile_number,
      userMsg: this.user_msg
    }

    this.auth.sendUserData(obj).subscribe((data: any) => {
      if (data['code'] == 200) {
        this.isNotSuccess = false;
        this.isSuccess = true;
        this.emptyFrom();
      } else {
        this.isNotSuccess = true;
        this.isSuccess = false
      }
    })
  }

  emptyFrom() {
    this.user_fullName = '';
    this.user_email = '';
    this.user_subject = '';
    this.user_mobile_number = '';
    this.user_msg = '';
  }



}
