
function showToastr(message, title, color, position, closeButton, progressBar){

    if(color == undefined || color == ''){
        color = 'success';
    }

    if(closeButton == undefined || closeButton == ''){
        closeButton = false;
    }
    if(progressBar == undefined || progressBar == ''){
        progressBar = false;
    }

    if(position == 'top-right'){
        position = 'toast-top-right';
    }else if(position == 'bottom-right'){
        position = 'toast-bottom-right';
    }else if(position == 'bottom-left'){
        position = 'toast-bottom-left';
    }else if(position == 'top-left'){
        position = 'toast-top-left';
    }else if(position == 'top-full'){
        position = 'toast-top-full-width';
    }else if(position == 'bottom-full'){
        position = 'toast-bottom-full-width';
    }else if(position == 'top-center'){
        position = 'toast-top-center';
    }else if(position == 'bottom-center'){
        position = 'toast-bottom-center';
    }

    toastr.options = {
        closeButton: closeButton,
        debug: false,
        newestOnTop: true,
        progressBar: progressBar,
        positionClass: position || 'toast-top-right',
        preventDuplicates: false,
        onclick: null
    };


    var $toast = toastr[color](message, title);
    $toastlast = $toast;
}
