
var member_container;

$(document).ready(() => {
    loadmemberPageContainer();
})
function getmembers() {
    return $.ajax({
        url: "member/getMemberData",
        dataType: "json",
    });
}
function loadmemberPageContainer() {
    $('#loadingModal').modal('show');
    $.when(
        getmembers()
    )
        .done(function (p1) {
            loadmembersView(p1);
            setTimeout(() => {
                $('#loadingModal').modal('hide');
            }, 500);
        }).fail(function (xhr) {
            $('#loadingModal').modal('hide');
            if (!isDefaultHandledAjaxError(xhr)) {
                const msg = 'Unexpected Server Exception. ';
                showToastr(msg, 'Page Loading Failed', 'error');
            }
        });
}


function loadmembersView(data) {
    member_container = data['Payload'];
    let stmt = '';
    $.each(data['Payload'], function (index, value) {
        let edit_id = 'edit|' + value['id']

        stmt += '   <div class="col-sm-6 col-md-3 mb-sm-30 text-center">' +
            '<div class="team-member bg-light pt-10 pb-15">' +
            '<div class="thumb"><img src="' + value['memberImageUrl'] + '" style="width:80%; height:auto">' +
            '</div>' +
            '<div class="info">' +
            '<div class="pt-10 pb-10 bg-theme-colored">' +
            ' <h4 class="header-title">' + value['memberName'] + '</h4>' +
            ' <h6 class="header-title">' + value['memberPost'] + '</h6>' +
            '</div>' +
            '</div>' +
            '<button type="submit" id="' + edit_id + '" class="btn btn-info waves-effect waves-light mb-2 " onClick="editmemberContent(this.id)">Edit</button>' +
            '<button type="submit" id="' + edit_id + '" class="btn btn-danger waves-effect waves-light mb-2 ml-2" onClick="deletememberContent(this.id)">Delete</button>' +
            '</div>' +
            '</div>'
    });

    $('#membersDetails').html('')
    $('#membersDetails').append(stmt)
}
function getmemberDetails(data) {
    $('#loadingModal').modal('show');
    return $.ajax({
        type: "POST",
        url: "member/addMember",
        data: data,
        dataType: "json",
        processData: false,
        contentType: false,
        success: function (data) {
            showToastr("You successfully Add One member", 'member', 'success');
            loadmemberPageContainer();
            setTimeout(() => {
                $('#loadingModal').modal('hide');
            }, 500);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            $('#loadingModal').modal('hide');
            showToastr("Somthing Went Wrong", 'member', 'error');
        }
    });
}

$("#member_from").validate({
    submitHandler: function (form) {
        var form_btn = $(form).find('button[type="submit"]');
        var form_result_div = '#form-result';
        $(form_result_div).remove();
        form_btn.before(
            '<div id="form-result" class="alert alert-success" role="alert" style="display: none;"></div>'
        );
        var form_btn_old_msg = form_btn.html();
        form_btn.html(form_btn.prop('disabled', true).data("loading-text"));

        var member_form_data = document.getElementById('member_from')
        let member_image = document.getElementById("memberImage").files[0];
        let member_obj = {}
        member_obj['memberName'] = member_form_data.member_name.value;
        member_obj['memberPost'] = member_form_data.member_post.value;
        member_obj['id'] = member_form_data.id.value;

        if (member_container.length != 0) {
            $.each(member_container, function (index, value) {
                if (value.id === member_form_data.id.value) {
                    member_obj['memberImageName'] = value['memberImageName'];
                    member_obj['memberImageUrl'] = value['memberImageUrl'];
                    member_obj['memberImageBase'] = value['memberImageBase'];
                }
            })
        }
        let formData = new FormData();
        formData.append("memberImage", member_image);
        formData.append("memberBody", JSON.stringify(member_obj));
        getmemberDetails(formData)
        form_btn.prop('disabled', false).html(form_btn_old_msg);
        document.getElementById("member_from").reset();
        $('#member_img_uri').attr('src', 'public/assets/images/upload.jpg');
    }
});


function editmemberContent(id) {
    console.log(id)
    edit_id = id.split('|');
    let member_data_for_edit;
    $.each(member_container, function (index, value) {
        if (value.id === edit_id[1]) {
            member_data_for_edit = value
        }
    })
    document.getElementById("member_from").reset();

    var member_form_data = document.getElementById('member_from')
    member_form_data.member_name.value = member_data_for_edit.memberName
    member_form_data.member_post.value = member_data_for_edit.memberPost
    member_form_data.id.value = member_data_for_edit.id
    // $('#member_img_uri').attr('src', member_data_for_edit.memberImageUrl);
    fetch(member_data_for_edit.memberImageUrl)
        .then(async response => {
            const contentType = response.headers.get('content-type')
            const blob = await response.blob()
            const file = new File([blob], member_data_for_edit.memberImageName, { contentType })

            let fileInputElement = document.getElementById('memberImage');
            let container = new DataTransfer();
            container.items.add(file);
            fileInputElement.files = container.files;
            memberImageChange(fileInputElement)
        })
    $('html, body').animate({
        scrollTop: $("#member_editor").offset().top
    }, 1000);

}
function memberImageChange(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#member_img_uri').attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}

function deletememberContent(id) {
    edit_id = id.split('|');

    let member_data_for_edit;
    $.each(member_container, function (index, value) {
        if (value.id === edit_id[1]) {
            member_data_for_edit = value
        }
    })
    $('#loadingModal').modal('show');
    return $.ajax({
        type: "DELETE",
        url: "member/deleteMember",
        data: member_data_for_edit,
        dataType: "json",
        success: function (data) {
            showToastr("You successfully Delete One member", 'member', 'success');
            loadmemberPageContainer();
            setTimeout(() => {
                $('#loadingModal').modal('hide');
            }, 500);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            $('#loadingModal').modal('hide');
            showToastr("Somthing Went Wrong", 'member', 'error');
        }
    });

}