
let testimonial_container;

$(document).ready(function () {
    loadTestimonialPageContentPage();
});
function clientLogoChange(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#client_img_uri').attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}
function getTestimonialContent() {
    return $.ajax({
        url: "testimonial/getTestimonialDetails",
        dataType: "json",
    });
}

function loadTestimonialPageContentPage() {
    // $('#loadingModal').modal('show');
    $.when(
        getTestimonialContent()
    )
        .done(function (p1) {
            loadTestimonialContentView(p1);
            setTimeout(() => {
                $('#loadingModal').modal('hide');
            }, 500);
        }).fail(function (xhr) {
            $('#loadingModal').modal('hide');
            if (!isDefaultHandledAjaxError(xhr)) {
                const msg = 'Unexpected Server Exception. ';
                showToastr(msg, 'Page Loading Failed', 'error');
            }
        });
}
function loadTestimonialContentView(data) {
    testimonial_container = data['Payload'];
    let stmt = '';
    stmt += '<table class="table">' +
        '<tr>' +
        '<th>Name</th>' +
        '<th>Post</th>' +
        '<th>Message</th>' +
        '<th>Image</th>' +
        '</tr>' +
        '<tboby>';

    $.each(data['Payload'], function (index, value) {
        let edit_id = 'edit|' + value['id']
        let delete_id = 'delete|' + value['id']
        stmt += '<tr><td>' + value['testimonialClientName'] + '</td>' +
            '<td>' + value['testimonialClientPost'] + '</td>' +
            '<td>' + value['testimonialMessage'] + '</td>' +
            '<td><img src="' + value['testimonialImageUrl'] + '" style="width:50%;height:auto"></td>' +
            '<td>' +
            '<button type="submit" id="' + edit_id + '" class="btn btn-info waves-effect waves-light mt-20 " onClick="edittestimonialContent(this.id)">Edit</button>' +
            '<button type="submit" id="' + edit_id + '" class="btn btn-danger waves-effect waves-light mt-20 ml-2" onClick="deletetestimonialContent(this.id)">Delete</button>' +
            '</td>' +
            '</tr>';
    })

    stmt += '</tbody>' +
        '</table >';

    $('#testimonialDetails').html('')
    $('#testimonialDetails').append(stmt)

}

$("#testimonial_info_form").validate({
    submitHandler: function (form) {
        var form_btn = $(form).find('button[type="submit"]');
        var form_result_div = '#form-result';
        $(form_result_div).remove();
        form_btn.before(
            '<div id="form-result" class="alert alert-success" role="alert" style="display: none;"></div>'
        );

        var testimonial_info_form = document.getElementById('testimonial_info_form')
        var testimonial_image = document.getElementById("client_img").files[0];
        let testimonialBoby = {};

        testimonialBoby['testimonialMessage'] = testimonial_info_form.client_msg.value,
            testimonialBoby['testimonialClientName'] = testimonial_info_form.client_name.value,
            testimonialBoby['testimonialClientPost'] = testimonial_info_form.client_post.value,
            testimonialBoby['id'] = testimonial_info_form.id.value

        if (testimonial_container.length != 0) {
            $.each(testimonial_container, function (index, value) {
                if (value.id === testimonial_info_form.id.value) {
                    testimonialBoby['testimonialImageName'] = value['testimonialImageName'];
                    testimonialBoby['testimonialImageUrl'] = value['testimonialImageUrl'];
                }
            })
        }

        let formData = new FormData();
        formData.append("testimonialImage", testimonial_image);
        formData.append("testimonialBody", JSON.stringify(testimonialBoby));
        posttestimonialContent(formData)
        document.getElementById("testimonial_conten_form").reset();
        $('#testimonial_img_uri').attr('src', 'public/assets/images/upload.jpg');
    }
});


function posttestimonialContent(data) {
    $('#loadingModal').modal('show');
    return $.ajax({
        type: "POST",
        url: "testimonial/setTestimonialDetails",
        data: data,
        dataType: "json",
        processData: false,
        contentType: false,
        success: function (data) {
            showToastr("Successfully Add testimonial Content", 'testimonial Page', 'success');
            loadTestimonialPageContentPage();
            setTimeout(() => {
                $('#loadingModal').modal('hide');
            }, 500);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            $('#loadingModal').modal('hide');
            showToastr("Somthing Went Wrong", 'testimonial Page', 'error');
        }
    });
}

function edittestimonialContent(id) {
    console.log(id)
    edit_id = id.split('|');
    let testimonial_data_for_edit;
    $.each(testimonial_container, function (index, value) {
        if (value.id === edit_id[1]) {
            testimonial_data_for_edit = value
        }
    })
    document.getElementById('testimonial_info_form').reset();
    var testimonialFormData = document.getElementById('testimonial_info_form')
    testimonialFormData.client_msg.value = testimonial_data_for_edit.testimonialMessage
    testimonialFormData.client_name.value = testimonial_data_for_edit.testimonialClientName
    testimonialFormData.client_post.value = testimonial_data_for_edit.testimonialClientPost
    testimonialFormData.id.value = testimonial_data_for_edit.id
    fetch(testimonial_data_for_edit.testimonialImageUrl)
        .then(async response => {
            const contentType = response.headers.get('content-type')
            const blob = await response.blob()
            const file = new File([blob], testimonial_data_for_edit.testimonialImageName, { contentType })

            let fileInputElement = document.getElementById('client_img');
            let container = new DataTransfer();
            container.items.add(file);
            fileInputElement.files = container.files;
            clientLogoChange(fileInputElement)
        })

    $('html, body').animate({
        scrollTop: $("#testimonial_editor").offset().top
    }, 1000);

}

function deletetestimonialContent(id) {

    edit_id = id.split('|');
    let testimonial_data_for_edit;
    $.each(testimonial_container, function (index, value) {
        if (value.id === edit_id[1]) {
            testimonial_data_for_edit = value
        }
    })
    // $('#loadingModal').modal('show')
    return $.ajax({
        type: "DELETE",
        url: "testimonial/deletTetestimonial",
        data: testimonial_data_for_edit,
        dataType: "json",
        success: function (data) {
            showToastr("Successfully Deleted testimonial Content", 'testimonial Page', 'success');
            loadTestimonialPageContentPage();
            setTimeout(() => {
                $('#loadingModal').modal('hide');
            }, 500);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            $('#loadingModal').modal('hide');
            showToastr("Somthing Went Wrong", 'testimonial Page', 'error');
        }
    });

}
