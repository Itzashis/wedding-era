
var video_container;

$(document).ready(() => {
    loadAllVideoContent();
})
function getVideos() {
    return $.ajax({
        url: "gallery/getAllVideos",
        dataType: "json",
    });
}
function loadAllVideoContent() {
    // $('#loadingModal').modal('show');
    $.when(
        getVideos()
    )
        .done(function (p1) {
            loadVideosViews(p1);
            setTimeout(() => {
                $('#loadingModal').modal('hide');
            }, 500);
        }).fail(function (xhr) {
            $('#loadingModal').modal('hide');
            if (!isDefaultHandledAjaxError(xhr)) {
                const msg = 'Unexpected Server Exception. ';
                showToastr(msg, 'Page Loading Failed', 'error');
            }
        });
}

function loadVideosViews(data) {
    video_container = data['Payload'];
    let stmt = '';
    $.each(data['Payload'], function (index, value) {
        let edit_id = 'edit|' + value['videoId']
        stmt += '<div class="col-sm-6 col-md-3 mb-sm-3 text-center">' +
            '<div class="pt-10 pb-10 bg-theme-colored">' +
            '<iframe width="100%" height="auto" src="' + value['videoLink'] + '"></iframe>' +
            ' <h4 class="header-title">' + value['videoName'] + '</h4>' +
            '</div>' +
            '<button type="submit" id="' + edit_id + '" class="btn btn-danger waves-effect waves-light mb-2 ml-2" onClick="deletevideoContent(this.id)">Delete</button>' +
            '</div>' +
            '</div>'
    });

    $('#videoDetails').html('')
    $('#videoDetails').append(stmt)
}
function getvideoDetails(data) {
    $('#loadingModal').modal('show');
    return $.ajax({
        type: "POST",
        url: "gallery/addNewVideo",
        data: data,
        dataType: "json",
        success: function (data) {
            showToastr("You successfully Add Video", 'Video Gallery', 'success');
            loadAllVideoContent();
            setTimeout(() => {
                $('#loadingModal').modal('hide');
            }, 500);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            $('#loadingModal').modal('hide');
            showToastr("Somthing Went Wrong", 'Video Gallery', 'error');
        }
    });
}

$("#video_from").validate({
    submitHandler: function (form) {
        var form_btn = $(form).find('button[type="submit"]');
        var form_result_div = '#form-result';
        $(form_result_div).remove();

        var video_form_data = document.getElementById('video_from')
        let video_obj = {}
        video_obj['videoName'] = video_form_data.video_name.value;
        video_obj['videoLink'] = video_form_data.video_Url.value;
        getvideoDetails(video_obj)
        document.getElementById("video_from").reset();
        $('#video_img_uri').attr('src', 'public/assets/images/upload.jpg');
    }
});


function deletevideoContent(id) {
    edit_id = id.split('|');
    let video_data_for_edit;
    $.each(video_container, function (index, value) {
        if (value.videoId === edit_id[1]) {
            video_data_for_edit = value
        }
    })
    $('#loadingModal').modal('show');
    return $.ajax({
        type: "DELETE",
        url: "gallery/deleteVideo",
        data: video_data_for_edit,
        dataType: "json",
        success: function (data) {
            showToastr("You successfully Delete One video", 'VIdeo Gallery', 'success');
            loadAllVideoContent();
            setTimeout(() => {
                $('#loadingModal').modal('hide');
            }, 500);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            $('#loadingModal').modal('hide');
            showToastr("Somthing Went Wrong", 'video', 'error');
        }
    });

}