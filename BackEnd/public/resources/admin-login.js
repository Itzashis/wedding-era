

$(document).ready(function () {
    // $('form').parsley();
    // loadLoginPage();
    let currentadminuser = localStorage.getItem("currentadminuser")
    if (currentadminuser) {
        window.location = '/dashboard'
        return true
    }
});


$("#login_form").validate({
    submitHandler: function (form) {
        var form_btn = $(form).find('button[type="submit"]');
        var form_result_div = '#form-result';
        $(form_result_div).remove();
        form_btn.before(
            '<div id="form-result" class="alert alert-success" role="alert" style="display: none;"></div>'
        );
        var user_info_form = document.getElementById('login_form')
        let userBoby = {
            'userEmail': user_info_form.userEmail.value,
            'password': user_info_form.password.value
        };



        if (userBoby.userEmail == 'admin@admin.com' && userBoby.password == '12345') {
            var user = localStorage.getItem("currentadminuser")
            if (user) {
                localStorage.removeItem("currentadminuser");
                localStorage.setItem("currentadminuser", JSON.stringify(userBoby));
            } else {
                localStorage.setItem("currentadminuser", JSON.stringify(userBoby));
            }
            window.location = '/'
        } else {
            let stmt = '<div class="alert alert-danger" role="alert">' + '<strong> User Email or Password is  Wrong</strong></div >'
            $('#alert_message_users').html('')
            $('#alert_message_users').append(stmt)
            setTimeout(() => {
                $('#alert_message_users').html('')
            }, 4000);
        }

        // $(form).ajaxSubmit({
        //     type: "POST",
        //     url: globalEnviroment.baseUrl + "user/login",
        //     data: userBoby,
        //     dataType: "json",
        //     success: function (data) {
        //         if (data.code == 402) {
        //             let stmt = '<div class="alert alert-danger" role="alert">' + '<strong>' + data.message + '</strong></div >'
        //             $('#alert_message_users').html('')
        //             $('#alert_message_users').append(stmt)
        //             setTimeout(() => {
        //                 $('#alert_message_users').html('')
        //             }, 4000);
        //         } else {
        //             var user = localStorage.getItem("currentuser")
        //             if (user) {
        //                 localStorage.removeItem("currentuser");
        //                 localStorage.setItem("currentuser", JSON.stringify(data['Payload']));
        //             } else {
        //                 localStorage.setItem("currentuser", JSON.stringify(data['Payload']));
        //             }

        //             window.location = '/user';
        //             setTimeout(() => {
        //                 $(form_result_div).fadeOut('slow')
        //             }, 4000);

        //             $(form).find('.form-control').val('');
        //         }

        //         form_btn.prop('disabled', false).html(form_btn_old_msg);
        //     },
        //     error: function (XMLHttpRequest, textStatus, errorThrown) {
        //         let stmt = '<div class="alert alert-danger" role="alert">' + '<strong> Somthing Went Wrong</strong></div >'
        //         $('#alert_message_users').html('')
        //         $('#alert_message_users').append(stmt)
        //         setTimeout(() => {
        //             $('#alert_message_users').html('')
        //         }, 2000);
        //     }
        // });
    }
});
