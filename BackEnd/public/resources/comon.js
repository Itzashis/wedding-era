


var gatewayBasrUrl;

function getFlag(priority) {
    priority = priority.toLowerCase();
    var flag = '';
    switch (priority) {
        case "p1":
            flag = '<i class="fa fa-lg fa-flag color-dark-red"></i>';
            break;
        case "p2":
            flag = '<i class="fa fa-lg fa-flag color-dark-brown"></i>';
            break;
        case "p3":
            flag = '<i class="fa fa-lg fa-flag color-pale-yellow"></i>';
            break;
        case "p4":
            flag = '<i class="fa fa-lg fa-flag color-dark-green"></i>';
            break;
        case "p5":
            flag = '<i class="fa fa-lg fa-flag color-pale-blue"></i>';
            break;

    }
    return flag;
}

function getFlag2x(priority) {
    priority = priority.toLowerCase();
    var flag = '';
    switch (priority) {
        case "p1":
            flag = '<i class="fa fa-2x fa-flag color-dark-red"></i>';
            break;
        case "p2":
            flag = '<i class="fa fa-2x fa-flag color-dark-brown"></i>';
            break;
        case "p3":
            flag = '<i class="fa fa-2x fa-flag color-pale-yellow"></i>';
            break;
        case "p4":
            flag = '<i class="fa fa-2x fa-flag color-dark-green"></i>';
            break;
        case "p5":
            flag = '<i class="fa fa-2x fa-flag color-pale-blue"></i>';
            break;

    }
    return flag;
}

function getDateNow() {
    var currDate = new Date();
    var todayDate = currDate.getFullYear() + "-"
        + leftPad('0', 2, (currDate.getMonth() + 1)) + "-"
        + leftPad('0', 2, (currDate.getDate()));
    return todayDate;
}

function getLastDate(interval) {
    var currDate = new Date();
    currDate.setDate(currDate.getDate() - interval);
    var date = currDate.getFullYear() + "-"
        + leftPad('0', 2, (currDate.getMonth() + 1)) + "-"
        + leftPad('0', 2, (currDate.getDate()));
    return date;
}

function transformCamelCaseToRegularForm(camelCaseWord) {
    return camelCaseWord.replace(/([A-Z])/g, ' $1').replace(/^./, function (str) { return str.toUpperCase(); });
}

function getDateTimeNow() {
    var currDate = new Date();
    var todayDate = currDate.getFullYear() + "-"
        + leftPad('0', 2, (currDate.getMonth() + 1)) + "-"
        + leftPad('0', 2, (currDate.getDate())) + " "
        + leftPad('0', 2, (currDate.getHours())) + ":"
        + leftPad('0', 2, (currDate.getMinutes())) + ":"
        + leftPad('0', 2, (currDate.getSeconds()));
    return todayDate;
}

function getTimeNow() {
    var currDate = new Date();
    var todayDate = leftPad('0', 2, (currDate.getHours())) + ":"
        + leftPad('0', 2, (currDate.getMinutes())) + ":"
        + leftPad('0', 2, (currDate.getSeconds()));
    return todayDate;
}

function getLabel(attr_name) {
    var label_name = '';
    if (attr_name != undefined) {
        if (attr_name.indexOf("-") > 0) {
            attr_name = attr_name.replace(/-/g, ' ');
        } else if (attr_name.indexOf("_") > 0) {
            attr_name = attr_name.replace(/_/g, ' ');
        } else {
            attr_name = attr_name.replace(/([A-Z])/g, " $1")
        }
        label_name = capitaliseWord(attr_name);
    }
    return label_name;
}

function capitaliseWord(content) {
    var ret = '';
    var text = content,
        tokens = text.split(" ").filter(function (t) {
            return t != "";
        }),
        res = [],
        i,
        len,
        component;
    for (i = 0, len = tokens.length; i < len; i++) {
        component = tokens[i];
        res.push(component.substring(0, 1).toUpperCase());
        res.push(component.substring(1));
        res.push(" "); // put space back in
    }
    ret += res.join("");
    return ret;
}

function getDatePicketFormat() {
    return 'yy-mm-dd';
}

function leftPad(padChar, size, value) {

    if (padChar.length != 1) {
        return;
    }

    if (value === undefined) {
        return;
    }

    var valueLn = value.toString().length;

    for (var i = valueLn; i < size; i++) {
        value = padChar + value;
    }
    return value;
}


function dateDiff(date1, date2, interval) {
    var second = 1000, minute = second * 60, hour = minute * 60, day = hour * 24, week = day * 7;
    date1 = new Date(date1);
    date2 = new Date(date2);
    var timediff = date2 - date1;
    if (isNaN(timediff)) return NaN;
    switch (interval) {
        case "years":
            return date2.getFullYear() - date1.getFullYear();
        case "months":
            return (
                (date2.getFullYear() * 12 + date2.getMonth())
                -
                (date1.getFullYear() * 12 + date1.getMonth())
            );
        case "weeks":
            return Math.floor(timediff / week);
        case "days":
            return Math.floor(timediff / day);
        case "hours":
            return Math.floor(timediff / hour);
        case "minutes":
            return Math.floor(timediff / minute);
        case "seconds":
            return Math.floor(timediff / second);
        default:
            return undefined;
    }
}

function ifNullUndefined(data) {
    if ((data == null) || (data == undefined)) {
        return " ";
    }
    return data;
}


function getFormattedDate(dateTime) {

    var yyyy = dateTime.substring(0, 4);
    var mm = dateTime.substring(5, 7);
    var dd = dateTime.substring(8, 10);
    return dd + " " + getMonthSemi(mm) + ", " + yyyy;
}


function getFormattedDateTime(dateTime) {

    var yyyy = dateTime.substring(0, 4);
    var mm = dateTime.substring(5, 7);
    var dd = dateTime.substring(8, 10);
    return dd + " " + getMonthSemi(mm) + ", " + yyyy + " " + dateTime.substring(11);
}

function getMonthSemi(mm) {
    var month;
    switch (mm) {
        case "01":
            month = "Jan";
            break;
        case "02":
            month = "Feb";
            break;
        case "03":
            month = "Mar";
            break;
        case "04":
            month = "Apr";
            break;
        case "05":
            month = "May";
            break;
        case "06":
            month = "Jun";
            break;
        case "07":
            month = "Jul";
            break;
        case "08":
            month = "Aug";
            break;
        case "09":
            month = "Sep";
            break;
        case "10":
            month = "Oct";
            break;
        case "11":
            month = "Nov";
            break;
        case "12":
            month = "Dec";
            break;
    }
    return month;
}

function getMonthAsNumber(month) {
    var monthNumber;
    switch (month) {
        case "Jan":
            monthNumber = "01";
            break;
        case "Feb":
            monthNumber = "02";
            break;
        case "Mar":
            monthNumber = "03";
            break;
        case "Apr":
            monthNumber = "04";
            break;
        case "May":
            monthNumber = "05";
            break;
        case "Jun":
            monthNumber = "06";
            break;
        case "Jul":
            monthNumber = "07";
            break;
        case "Aug":
            monthNumber = "08";
            break;
        case "Sep":
            monthNumber = "09";
            break;
        case "Oct":
            monthNumber = "10";
            break;
        case "Nov":
            monthNumber = "11";
            break;
        case "Dec":
            monthNumber = "12";
            break;
    }
    return monthNumber;
}

function dateToString(date) {
    var month = '' + (date.getMonth() + 1);
    if (month.length < 2) month = '0' + month;
    var day = '' + date.getDate();
    if (day.length < 2) day = '0' + day;
    var year = date.getFullYear();
    return [year, month, day].join('-');
}

function capitalize(text) {
    text = text.toLowerCase().replace(/\b[a-z]/g, function (letter) {
        return letter.toUpperCase();
    });
    return text;
}

jQuery.fn.extend({
    capitalize: function () {
        var text = $(this).text();
        text = text.toLowerCase().replace(/\b[a-z]/g, function (letter) {
            return letter.toUpperCase();
        });
        return text;
    }
});

function formatMillisToWideFormat(millis) {

    var yyyy = millis.substring(0, 4);
    var MM = millis.substring(4, 6);
    var dd = millis.substring(6, 8);
    var hh = millis.substring(8, 10);
    var mm = millis.substring(10, 12);
    var ss = millis.substring(12, 14);

    return yyyy + "-" + MM + "-" + dd + " " + hh + ":" + mm + ":" + ss;
}

function reformatNumberWithThousandSeparator(value) {
    if ($.isNumeric(value)) {
        return Number(value).toLocaleString();
    }
    return value;
}

function reformatNumber(value) {
    var newValue = value;
    if (value >= 1000) {
        var suffixes = ["", "K", "M", "B", "T"];
        var suffixNum = Math.floor(("" + value).length / 3);
        var shortValue = '';
        for (var precision = 2; precision >= 1; precision--) {
            shortValue = parseFloat((suffixNum != 0 ? (value / Math.pow(1000, suffixNum)) : value).toPrecision(precision));
            var dotLessShortValue = (shortValue + '').replace(/[^a-zA-Z 0-9]+/g, '');
            if (dotLessShortValue.length <= 2) { break; }
        }
        if (shortValue % 1 != 0) shortNum = shortValue.toFixed(1);
        newValue = shortValue + suffixes[suffixNum];
    }
    return newValue;
}

function formatBytes(bytes, decimals) {
    if (bytes == 0) return '0 Byte';
    var k = 1024;
    var dm = decimals;
    if ((decimals == undefined) || (decimals == '')) {
        dm = 2;
    }
    var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
    var i = Math.floor(Math.log(bytes) / Math.log(k));
    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
}

function formatKiloBytes(kiloBytes, decimals) {
    if (kiloBytes == 0) return '0 KB';
    var k = 1024;
    var dm = decimals;
    if ((decimals == undefined) || (decimals == '')) {
        dm = 2;
    }
    var sizes = ['KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
    var i = Math.floor(Math.log(kiloBytes) / Math.log(k));
    return parseFloat((kiloBytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
}

function convertKilobytesIntoBytes(kiloBytes) {
    return parseFloat(kiloBytes) * 1024
}
function isEmptyOrNullOrUndefined(data) {
    if ((data == null) || (data == undefined) || (Object.prototype.toString.call(data) == '[object String]' && data.trim() == '')) {
        return true;
    }
    return false;
}

function getShiftedLatLong(portToLat, portToLong, baseLat, baseLong, srcLat, srcLong) {

    var diffLat = srcLat - baseLat;
    var diffLong = srcLong - baseLong;

    var shiftLat = portToLat + diffLat;
    var shiftLong = portToLong + diffLong;

    var map = {};
    map["shiftLat"] = shiftLat;
    map["shiftLong"] = shiftLong;

    return map;
}


function getAppCurrency() {
    return '$';
}

function loadAppCurrency() {
    $(".app-currency").text(getAppCurrency());
}

function ajaxHandleUserSubscription(resourceId, resourceType, hasSubscribed) {
    var url = "";
    if (hasSubscribed == "true") url = "unSubscribeResource?resourceId=" + reportId + "&resourceType=" + resourceType;
    else url = "subscribeResource?resourceId=" + reportId + "&resourceType=" + resourceType;

    return $.ajax({
        type: "POST",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        url: url,
        data: JSON.stringify({
        }),
        dataType: "json"
    });
}



//Dynamic Menu
// getListOfAccessibleMenuItems();

function getListOfAccessibleMenuItems() {
    $.ajax({
        type: "GET",
        url: "getAccessibleMenuItems",
        dataType: "json",
        success: function (data) {
            if (data['returnCode'] == '200') {
                $.each(data['Payload'], function (index, menuItemIds) {
                    $.each(menuItemIds, function (index, menuItem) {

                        if (menuItem.indexOf('dashboard') < 0) {
                            $("#menu_dashboard").hide();
                        }

                        if (menuItem.indexOf('kpiBrowser') < 0 && menuItem.indexOf('kpiBookmarks') < 0) {
                            $("#menu_kpi").hide();
                        }
                        if (menuItem.indexOf('kpiBrowser') < 0) {
                            $("#menu_kpiBrowser").hide();
                        }
                        if (menuItem.indexOf('kpiBookmarks') < 0) {
                            $("#menu_kpiBookmarks").hide();
                        }

                        if (menuItem.indexOf('reportKpiBrowser') < 0 && menuItem.indexOf('reportsKpiBookmarks') < 0) {
                            $("#menu_reports").hide();
                        }
                        if (menuItem.indexOf('reportKpiBrowser') < 0) {
                            $("#menu_reportKpiBrowser").hide();
                        }
                        if (menuItem.indexOf('reportsKpiBookmarks') < 0) {
                            $("#menu_reportsKpiBookmarks").hide();
                        }

                        if (menuItem.indexOf('trendKpiBrowser') < 0 && menuItem.indexOf('trendKpiBookmarks') < 0) {
                            $("#menu_trend").hide();
                        }
                        if (menuItem.indexOf('trendKpiBrowser') < 0) {
                            $("#menu_trendKpiBrowser").hide();
                        }
                        if (menuItem.indexOf('trendKpiBookmarks') < 0) {
                            $("#menu_trendKpiBookmarks").hide();
                        }

                        if (menuItem.indexOf('reconciliation') < 0) {
                            $("#menu_reconciliation").hide();
                        }

                        if (menuItem.indexOf('amlReports') < 0) {
                            $("#menu_amlReports").hide();
                        }

                        if (menuItem.indexOf('fileVault') < 0 && menuItem.indexOf('browser') < 0
                            && menuItem.indexOf('summary') < 0 && menuItem.indexOf('exception') < 0) {
                            $("#menu_sourceManagement").hide();
                        }
                        if (menuItem.indexOf('fileVault') < 0) {
                            $("#menu_fileVault").hide();
                        }
                        if (menuItem.indexOf('browser') < 0) {
                            $("#menu_browser").hide();
                        }
                        if (menuItem.indexOf('summary') < 0) {
                            $("#menu_summary").hide();
                        }
                        if (menuItem.indexOf('exception') < 0) {
                            $("#menu_exception").hide();
                        }

                        if (menuItem.indexOf('profile') < 0) {
                            $("#menu_profile").hide();
                        }

                        /*
                                                if(menuItem.indexOf('search') < 0){
                                                    $("#menu_search").hide();
                                                }
                        */

                        if (menuItem.indexOf('analysisStudio') < 0) {
                            $("#menu_analysisStudio").hide();
                        }

                        if (menuItem.indexOf('cdrBrowser') < 0) {
                            $("#menu_cdrBrowser").hide();
                        }

                        if (menuItem.indexOf('alertManager') < 0) {
                            $("#menu_alertManager").hide();
                        }

                        if (menuItem.indexOf('caseTrackerProxyUrl') < 0) {
                            $("#menu_caseTrackerProxyUrl").hide();
                        }

                        if (menuItem.indexOf('ruleCreation') < 0 && menuItem.indexOf('ruleViewer') < 0) {
                            $("#menu_rulesBrowser").hide();
                        }
                        if (menuItem.indexOf('ruleCreation') < 0) {
                            $("#menu_ruleCreation").hide();
                        }
                        if (menuItem.indexOf('ruleViewer') < 0) {
                            $("#menu_ruleViewer").hide();
                        }



                        if (menuItem.indexOf('reportsBuilder') < 0 && menuItem.indexOf('savedReports') < 0) {
                            $("#menu_reportGenerator").hide();
                        }
                        if (menuItem.indexOf('reportsBuilder') < 0) {
                            $("#menu_reportsBuilder").hide();
                        }
                        if (menuItem.indexOf('savedReports') < 0) {
                            $("#menu_savedReports").hide();
                        }




                        if (menuItem.indexOf('users') < 0 && menuItem.indexOf('userRoles') < 0) {
                            $("#menu_administration").hide();
                        }
                        if (menuItem.indexOf('users') < 0) {
                            $("#menu_users").hide();
                        }
                        if (menuItem.indexOf('userRoles') < 0) {
                            $("#menu_userRoles").hide();
                        }

                        if (menuItem.indexOf('system') < 0) {
                            $("#menu_system").hide();
                        }
                        if (menuItem.indexOf('api') < 0) {
                            $("#menu_api").hide();
                        }
                        if (menuItem.indexOf('adhocJobs') < 0) {
                            $("#menu_job").hide();
                        }
                        if (menuItem.indexOf('cache') < 0) {
                            $("#menu_cache").hide();
                        }
                        if (menuItem.indexOf('serviceClassConfiguration') < 0) {
                            $("#menu_serviceClassConfiguration").hide();
                        }
                        if (menuItem.indexOf('specialNumberConfiguration') < 0) {
                            $("#menu_specialNumberConfiguration").hide();
                        }
                    });
                });
            } else {
                showToastr('Msg :' + data['message'], 'Menu Initialisation', 'error');
            }
        }
    });
}



commonLoadOnPageInit();

function commonLoadOnPageInit() {
    extractTimeZoneOffset();

    $.ajaxSetup({
        normalizeForStatusCodes: true
    });

    $.ajaxPrefilter(
        function (options, localOptions, jqXHR) {
            if (options.normalizeForStatusCodes) {
                normalizeAJAXRequestForStatusCodes(jqXHR);
            }
        }
    );

    function normalizeAJAXRequestForStatusCodes(jqXHR) {

        var normalizedRequest = $.Deferred();

        jqXHR.then(
            // success hook
            normalizedRequest.resolve,
            // Fail hook
            function (jqXHR) {
                var msg;
                if (isDefaultHandledAjaxError(jqXHR)) {
                    if (jqXHR.status == 401) {
                        if ($("#login-flow-modal").data('bs.modal') != undefined) { // checks if modal opened
                            msg = getLoginErrorMessage(jqXHR.responseText);
                        } else {
                            msg = ''; // initialise to remove previous error messages
                        }
                        loadLoginFlowModal(msg);
                    }
                }
                normalizedRequest.rejectWith(
                    this,
                    [
                        jqXHR
                    ]
                );
            }
        );
        jqXHR = normalizedRequest.promise(jqXHR);
        jqXHR.success = jqXHR.done;
        jqXHR.error = jqXHR.fail;
    }
}

// errors are handled in function normalizeAJAXRequestForStatusCodes
function isDefaultHandledAjaxError(xhr) {
    return xhr.status == 401;
}
function loginFlowAction() {

    var data = 'username=' + $('#login-flow-username').text().trim() + '&password=' + $('#login-flow-password').val();
    $('#login-flow-password').val('');
    $.ajax({
        data: data,
        type: 'POST',
        url: gatewayBasrUrl + 'login'
    }).done(function (data, textStatus, jqXHR) {
        $("#login-flow-modal").modal('hide');
        var reloadPage = jqXHR.getResponseHeader('reload-page');
        if (reloadPage) {
            location.reload(true);
        }
    })/*.fail(function (jqXHR, textStatus, errorThrown) {
     var msg;
     if (jqXHR.status == 401) {
     msg = getLoginErrorMessage(jqXHR.responseText);
     } else {
     msg = "Unexpected error, please try again or contact system admin";
     }
     $('#login-flow-error').text(msg);
     });*/
}

function loadLoginFlowModal(txt) {
    $('#login-flow-error').text(txt);
    $('#login-flow-password').val('');
    $("#login-flow-modal").modal('show');
}

$(".login-flow-submit").click(function (e) {
    e.preventDefault();
    loginFlowAction();
});

function getLoginErrorMessage(msg) {

    var msgSplit = msg.split('HTTP Status 401 - ');
    var errorMsg;
    if (msgSplit.length > 1) {
        errorMsg = msgSplit[1].split('</h1>')[0];
    } else {
        errorMsg = "Unexpected error, please try again or contact system admin";
    }
    return errorMsg;
}

function getLoginErrorMessageFromErrorObject(jqXHR) {

    if (jqXHR.status === 401) {
        errorMsg = "Invalid credentials";
    } else {
        errorMsg = jqXHR.responseText;
        if ($.trim(errorMsg) === "") {
            errorMsg = "Unexpected error, please try again or contact system admin";
        }
    }
    return errorMsg;
}

function extractTimeZoneOffset() {
    var myDate = new Date();
    var offset = myDate.getTimezoneOffset() * -1;
    //var timeZone = moment.tz.guess();
    setTimeZoneOffsetInCookie(offset);
}

function setTimeZoneOffsetInCookie(offset) {
    document.cookie = "TIMEZONE_COOKIE=" + offset;
}

function getTimeZoneOffset() {
    var myDate = new Date();
    return myDate.getTimezoneOffset() * -1;
}

$(document).on('show.bs.modal', '#login-flow-modal', function (e) {
    var zIndex = Math.max.apply(null, Array.prototype.map.call(document.querySelectorAll('*'), function (el) {
        return +el.style.zIndex;
    })) + 10;
    $(this).css('z-index', zIndex);
});

$(document).on('show.bs.modal', '.modal', function (e) {

    if ($(document).height() <= $(window).height()) {
        $('body').css('margin-right', '0', '!important');
    }
    else {
        $('body').removeAttr('style');
    }
});
$('.modal').on('hide.bs.modal', function () {
    $('body').removeAttr('style');
});

/* Getting percentage Value for Reconciliation Page*/

/*
function getPercentageVal(data1, data2){
    if(data1 != 0){
        return (((data1 - data2)/data1)*100);
    }else {
        return "N.A.";
    }

}

function getColorCode(data){
    if(data <= 0){
        return "<i class='fa fa-lg fa-arrow-circle-up' style='color: forestgreen'></i>";
    }else if(data > 0 && data <= 100){
        return "<i class='fa fa-lg fa-arrow-circle-down' style='color: #ed1c24'></i>";
    }else if(data > 100 && data <= 500){
        return "<i class='fa fa-lg fa-arrow-circle-down' style='color: #B71C1C'></i>";
    }else if(data > 500){
        return "<i class='fa fa-lg fa-arrow-circle-down' style='color: #8e120a'></i>";
    }else if(data == "N.A.") {
        return "";
    }
}
*/

function getColorCodeSpan(data, colorCode) {
    if (data <= 0) {
        return "<i class='fa fa-lg fa-arrow-circle-up' style='color: " + colorCode + "'></i>";
    } else if (data > 0) {
        return "<i class='fa fa-lg fa-arrow-circle-down' style='color: " + colorCode + "'></i>";
    } else if (data == "N.A.") {
        return "";
    }
}

function roundOffTillTwo(value, exp) {
    if (typeof exp === 'undefined' || +exp === 0)
        return Math.round(value);

    value = +value;
    exp = +exp;

    if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0))
        return NaN;

    // Shift
    value = value.toString().split('e');
    value = Math.round(+(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp)));

    // Shift back
    value = value.toString().split('e');
    return +(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp));
}

function abbreviateNumber(number) {
    var SI_POSTFIXES = ["", "K", "M", "B", "T"];
    var tier = Math.log10(Math.abs(number)) / 3 | 0;
    if (tier == 0) return number;
    var postfix = SI_POSTFIXES[tier];
    var scale = Math.pow(10, tier * 3);
    var scaled = number / scale;
    var formatted = scaled.toFixed(1) + '';
    if (/\.0$/.test(formatted))
        formatted = formatted.substr(0, formatted.length - 2);
    return formatted + postfix;
}

function scaleNumber(inputY, yMin, yMax, xMin, xMax) {
    var percent = (inputY - yMin) / (yMax - yMin);
    return percent * (xMax - xMin) + xMin;
}

function spinner(parent_id, action) {
    if (action == "show") {
        $('#' + parent_id).find('.spinner-div').hide();
        $('#' + parent_id).find('.spinner').show();
    } else {
        $('#' + parent_id).find('.spinner').hide();
        $('#' + parent_id).find('.spinner-div').show();
    }
}

function navigateToSubscriberProfile(msisdn, imsi) {
    var url = "profile?accountNum=" + msisdn;
    if (imsi != undefined) {
        url += "&imsi=" + imsi;
    }
    open_link = window.open('', '_blank');
    open_link.location = url;
}
function getMenuIcons(menuNames) {
    if (menuNames == 'user') {
        return '<i class="fa fa-md fa-fw fa-user"></i>';
    } else if (menuNames == 'kpiBrowser') {
        return '<i class="fa fa-md fa-fw fa-shopping-cart"></i>';
    } else if (menuNames == 'reports') {
        return '<i class="fa fa-md fa-fw fa-indent"></i>';
    } else if (menuNames == 'trends') {
        return '<i class="fa fa-md fa-fw fa-line-chart"></i>';
    } else if (menuNames == 'reconciliation') {
        return '<i class="fa fa-md fa-fw fa-exchange"></i>';
    } else if (menuNames == 'rule') {
        return '<i class="fa fa-md fa-fw fa-object-group"></i>';
    } else if (menuNames == 'alerts') {
        return '<i class="fa fa-md fa-fw fa-bell-o"></i>';
    } else if (menuNames == 'sourceManagement') {
        return '<i class="fa fa-md fa-fw fa-cogs"></i>';
    } else if (menuNames == 'menu') {
        return '<i class="fa fa-md fa-fw fa-list"></i>';
    } else if (menuNames == 'dashboard') {
        return '<i class="fa fa-md fa-th-large"></i>';
    } else if (menuNames == 'thirdPartyApi') {
        return '<i class="fa fa-md fa-fw fa-arrows"></i>';
    } else if (menuNames == 'policy') {
        return '<i class="fa fa-md fa-fw fa-gavel"></i>';
    } else if (menuNames == "job") {
        return '<i class="fa fa-md fa-fw fa-briefcase"></i>';
    } else {
        return '<i class="fa fa-md fa-fw fa-tasks"></i>';
    }
}

function getDatasourceIcon(datasource) {

    var text = datasource.toLowerCase();
    switch (text) {
        case 'eric-msc':
            return '<i class="fa fa-phone" aria-hidden="true"></i>';
        case 'eric-gmsc':
            return '<i class="fa fa-phone" aria-hidden="true"></i>';
        case 'eric-ccn':
            return '<i class="fa fa-money" aria-hidden="true"></i>';
        case 'eric-air':
            return '<i class="fa fa-shopping-bag" aria-hidden="true"></i>';
        case 'eric-ggsn':
            return '<i class="fa fa-wifi" aria-hidden="true"></i>';
        case 'eric-sgsn':
            return '<i class="fa fa-wifi" aria-hidden="true"></i>';
        case 'tap':
            return '<i class="fa fa-share-alt" aria-hidden="true"></i>';
        case 'tapin':
            return '<i class="fa fa-share-alt" aria-hidden="true"></i>';
        case 'tapout':
            return '<i class="fa fa-share-alt" aria-hidden="true"></i>';
        case 'nrtrde':
            return '<i class="fa fa-superpowers" aria-hidden="true"></i>';
        case 'sdp-in':
            return '<i class="fa fa-money" aria-hidden="true"></i>';
        default:
            return '<i class="fa fa-file-code-o" aria-hidden="true"></i>';
    }
}

function getMonthDateRange(year, month) {
    //var moment = require('moment');

    // month in moment is 0 based, so 9 is actually october, subtract 1 to compensate
    // array is 'year', 'month', 'day', etc
    var startDate = moment([year, month - 1]);

    // Clone the value before .endOf()
    var endDate = moment(startDate).endOf('month');

    // just for demonstration:

    return { start: startDate, end: endDate };
}

function navigateProfileIcon() {
    var btnStmt = '<button class="btn btn-xs btn-primary navigateToSubscriberProfile" title="Navigate Profile">' +
        '<i class="fa fa-paper-plane"></i></button>';
    return btnStmt;
}

function subscribeFeedbackSubmission() {
    $.feedback({
        ajaxURL: 'captureUserResponse',
        postHTML: false
    });
}

function JSONToCSVConvertor(JSONData, ReportTitle, ShowLabel) {
    //If JSONData is not an object then JSON.parse will parse the JSON string in an Object
    var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;

    var CSV = '';
    //Set Report title in first row or line

    // CSV += ReportTitle + '\r\n\n';

    //This condition will generate the Label/Header
    if (ShowLabel) {
        var row = "";

        //This loop will extract the label from 1st index of on array
        for (var index in arrData[0]) {

            //Now convert each value to string and comma-seprated
            row += index + ',';
        }

        row = row.slice(0, -1);

        //append Label row with line break
        CSV += row + '\r\n';
    }

    //1st loop is to extract each row
    for (var i = 0; i < arrData.length; i++) {
        var row = "";

        //2nd loop will extract each column and convert it in string comma-seprated
        for (var index in arrData[i]) {
            row += '"' + arrData[i][index] + '",';
        }

        row.slice(0, row.length - 1);

        //add a line break after each row
        CSV += row + '\r\n';
    }

    if (CSV == '') {
        console.log("Invalid data");
        return;
    }

    //Generate a file name
    var fileName = "Report-";
    //this will remove the blank-spaces from the title and replace it with an underscore
    fileName += ReportTitle.replace(/ /g, "-");

    //Initialize file format you want csv or xls
    var uri = 'data:text/csv;charset=utf-8,' + escape(CSV);

    // Now the little tricky part.
    // you can use either>> window.open(uri);
    // but this will not work in some browsers
    // or you will not get the correct file extension

    //this trick will generate a temp <a /> tag
    var link = document.createElement("a");
    link.href = uri;

    //set the visibility hidden so it will not effect on your web-layout
    link.style = "visibility:hidden";
    link.download = fileName + ".csv";

    //this part will append the anchor tag and remove it after automatic click
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
}

function scaleNumber(inputY, yMin, yMax, xMin, xMax) {
    if (yMin == yMax) {
        if (inputY == yMin) {
            return xMax;
        }
        return xMin;
    } else {
        var percent = (inputY - yMin) / (yMax - yMin);
        return percent * (xMax - xMin) + xMin;
    }
}

function createHtml(width, data, color) {
    return '<div style="display: inline"> ' +
        '<div class="no-padding" style="float:left; width: ' + width + '%; background-color: ' + color + '; ' +
        'height: 15px; position: relative;"></div>' +
        '<div class="no-padding" style="float: left">' + data + '</div>' +
        '</div>';
}

function createVarianceHtmlForBytes(width, data1, data2) {
    var columnColor = "";
    var columnData = (data1 - data2);
    if (columnData > 0) {
        columnColor = "#e8ca09";
    } else {
        columnColor = "#db1500";
    }

    return '<div style="display: inline"> ' +
        '<div class="no-padding" style="float:left; width: ' + width + '%; background-color: ' + columnColor + '; ' +
        'height: 15px; position: relative;"></div>' +
        '<div class="no-padding" style="float: left">' + formatBytes(columnData) + '</div>' +
        '</div>';
}

function createVarianceHtmlForCount(width, data1, data2) {
    var columnColor = "";
    var columnData = (data1 - data2);
    if (columnData > 0) {
        columnColor = "#e8ca09";
    } else {
        columnColor = "#db1500";
    }

    return '<div style="display: inline"> ' +
        '<div class="no-padding" style="float:left; width: ' + width + '%; background-color: ' + columnColor + '; ' +
        'height: 15px; position: relative;"></div>' +
        '<div class="no-padding" style="float: left">' + reformatNumberWithThousandSeparator(columnData) + '</div>' +
        '</div>';
}

function formatMillisec(value) {
    var millisecInDay = 86400000;
    var millisecInHour = 3600000;
    var millisecInMin = 60000;
    var millisecInSec = 1000;

    if (value >= millisecInDay) {
        var dayValue = value / 86400000;
        var day = (dayValue).toFixed(1);
        if (day == 1) {
            return day + ' day'
        } else {
            return day + ' days'
        }
    }
    else if (value >= millisecInHour) {
        var hour = value / 3600000;
        return hour + ' hr'
    }
    else if (value >= millisecInMin) {
        var min = value / 60000;
        return min + ' min';
    }
    else if (value >= millisecInSec) {
        var sec = value / 1000;
        return sec + ' sec';
    }
    else if (value < 1000) {
        return value + ' mil'
    }
}

function getPercentage(value1, value2) {
    var percentage = '';
    // value1 = 4344569.9985;
    // value2 = 6558978.9445;
    if (value1 <= 0) {
        percentage += "NA"
    } else {
        percentage = ((roundOffTillTwo(value1, 2) - roundOffTillTwo(value2, 2)) * 100) / roundOffTillTwo(value1, 2);
    }
    // stmt += percentage + "<i class ='fa fa-clock'></i>";
    return percentage;
}

