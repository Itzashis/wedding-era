let image_container = [];
let gallery_main_container = [];

$(document).ready(function () {
    ImgUpload();
    loadGalleryPage();
});

function getallGalleryImage() {
    return $.ajax({
        url: "gallery/getAllImage",
        dataType: "json",
    });
}

function loadGalleryPage() {
    // $('#loadingModal').modal('show');
    $.when(
        getallGalleryImage()
    )
        .done(function (p1) {
            loadGalleryView(p1);
            gallery_main_container = p1['Payload']
            // setTimeout(() => {
            //     $('#loadingModal').modal('hide');
            // }, 500);

        }).fail(function (xhr) {
            $('#loadingModal').modal('hide');
            if (!isDefaultHandledAjaxError(xhr)) {
                const msg = 'Unexpected Server Exception. ';
                showToastr(msg, 'Page Loading Failed', 'error');
            }
        });
}



function postGalleryDataImage(data) {
    // $('#loadingModal').modal('show');
    return $.ajax({
        type: "POST",
        url: "gallery/addGallery",
        data: data,
        dataType: "json",
        processData: false,
        contentType: false,
        success: function (data) {
            if (data.code == 200) {
                showToastr("You successfully Add Gallery", 'Image Gallery', 'success');
                $('#newGalleryModle').modal('hide');
                image_container = [];
                loadGalleryPage();
                document.getElementById("add_new_image_form").reset();
            } else {
                showToastr("Somthing went wrong", 'Image Gallery', 'error');
            }
            // setTimeout(() => {
            //     $('#loadingModal').modal('hide');
            // }, 500);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            $('#loadingModal').modal('hide');
            showToastr("Somthing Went Wrong", 'Image Gallery', 'error');
        }
    });
}

function addNewImageInGallery(data) {
    // $('#loadingModal').modal('show');
    return $.ajax({
        type: "POST",
        url: "gallery/addNewImageInGallery",
        data: data,
        dataType: "json",
        processData: false,
        contentType: false,
        success: function (data) {
            if (data.code == 200) {
                showToastr("You successfully Add Image In Gallery", 'Image Gallery', 'success');
                $('#addnewImageInGalleryModle').modal('hide');
                image_container = [];
                loadGalleryPage();
                document.getElementById("member_from").reset();
            } else {
                showToastr("Somthing went wrong", 'Image Gallery', 'error');
            }
            // setTimeout(() => {
            //     $('#loadingModal').modal('hide');
            // }, 500);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            $('#loadingModal').modal('hide');
            showToastr("Somthing Went Wrong", 'Image Gallery', 'error');
        }
    });
}


$('#new_image_gallery').on('click', function (e) {
    $('#newGalleryModle').modal('show');
})

$('#existing_image_gallery').on('click', function (e) {
    $('#addnewImageInGalleryModle').modal('show');
})

function ImgUpload() {
    var imgWrap = "";
    var imgArray = [];

    $('.upload__inputfile').each(function () {
        $(this).on('change', function (e) {
            imgWrap = $(this).closest('.upload__box').find('.upload__img-wrap');
            var maxLength = $(this).attr('data-max_length');
            var files = e.target.files;
            var filesArr = Array.prototype.slice.call(files);
            var iterator = 0;

            filesArr.forEach(function (f, index) {

                if (!f.type.match('image.*')) {
                    return;
                }

                if (imgArray.length > maxLength) {
                    return false
                } else {
                    var len = 0;
                    for (var i = 0; i < imgArray.length; i++) {
                        if (imgArray[i] !== undefined) {
                            len++;
                        }
                    }
                    if (len > maxLength) {
                        return false;
                    } else {
                        imgArray.push(f);

                        var reader = new FileReader();
                        reader.onload = function (e) {
                            var html = "<div class='upload__img-box'><div style='background-image: url(" + e.target.result + ")' data-number='" + $(".upload__img-close").length + "' data-file='" + f.name + "' class='img-bg'><div class='upload__img-close'></div></div></div>";
                            imgWrap.append(html);
                            iterator++;
                        }
                        reader.readAsDataURL(f);
                    }
                }
            });
        });
    });

    $('body').on('click', ".upload__img-close", function (e) {
        var file = $(this).parent().data("file");
        for (var i = 0; i < imgArray.length; i++) {
            if (imgArray[i].name === file) {
                imgArray.splice(i, 1);
                break;
            }
        }
        $(this).parent().parent().remove();
    });
    image_container = imgArray
    $('body').on('click', ".close-modal-exist", function (e) {
        imgWrap.html('');
        imgArray = [];
        document.getElementById("add_new_image_form").reset();
    });
    $('body').on('click', ".close-modal-new", function (e) {
        imgWrap.html('');
        imgArray = [];
        document.getElementById("gallery_form").reset();
    });
    $('body').on('click', "#add_new_image_in_gallery", function (e) {
        imgWrap.html('');
        imgArray = [];
        document.getElementById("add_new_image_form").reset();
    });
    $('body').on('click', "#add_image_gallery", function (e) {
        imgWrap.html('');
        imgArray = [];
        document.getElementById("gallery_form").reset();
    });

}


$('#add_new_image_in_gallery').on('click', function (e) {

    var gallery_form = document.getElementById('add_new_image_form');
    var galleryBody = {
        galleryId: gallery_form.gal_name.value,
        galleryName: $('#gal_name').find(":selected").text()
    }
    let formData = new FormData();
    if (image_container.length) {
        for (let i = 0; i < image_container.length; i++)
            formData.append('addNewGalleyImage', image_container[i], image_container[i].name);
    }
    formData.append("galleryBody", JSON.stringify(galleryBody));
    addNewImageInGallery(formData)
})


$('#add_image_gallery').on('click', function (e) {

    var gallery_form = document.getElementById('gallery_form');
    var galleryBody = {
        galleryName: gallery_form.gal_name.value,
    }
    let formData = new FormData();
    if (image_container.length) {
        for (let i = 0; i < image_container.length; i++)
            formData.append('galleyImage', image_container[i], image_container[i].name);
    }
    formData.append("galleryBody", JSON.stringify(galleryBody));
    postGalleryDataImage(formData)
})

function loadGalleryView(data) {

    let formatted_gallery_data_map = {};
    let gallery_arry = []
    let gallery_drop_down_data = [];
    for (const [indx, statusMap] of Object.entries(data['Payload'])) {
        let gallery_display_name = statusMap['galleryName'];

        let gallery_data_array = undefined;
        if (!formatted_gallery_data_map.hasOwnProperty(gallery_display_name)) {
            gallery_data_array = [];
            formatted_gallery_data_map[gallery_display_name] = gallery_data_array;
        }
        gallery_data_array = formatted_gallery_data_map[gallery_display_name];
        let node = {
            'galleryId': statusMap['galleryId'],
            'imageName': statusMap['imageName'],
            'imageUrl': statusMap['imageUrl'],
            'imageBasePath': statusMap['imageBasePath'],
            'imageId': statusMap['imageId'],
        };
        gallery_data_array.push(node);
    }

    for (const [name, data_array] of Object.entries(formatted_gallery_data_map)) {
        gallery_arry.push({ 'galleryName': name, 'data': data_array });
        gallery_drop_down_data.push({
            galleryName: name,
            galleryId: data_array[0].galleryId
        })

    }

    let statement = '';
    statement += '<label>Select Name Of Gallery</label>' +
        '<select class="form-control" id="gal_name" required placeholder = "Name of your Gallery" >';

    $.each(gallery_drop_down_data, function (index, value) {
        statement += '<option value="' + value['galleryId'] + '">' + value['galleryName'] + '</option>'
    });
    statement += '</select>'
    $("#allGalleryDropdown").html('')
    $("#allGalleryDropdown").append(statement)


    let stmt = '';
    $.each(gallery_arry, function (index, value) {
        let add_image_gall_id = 'add_image|' + value['data'][0]['galleryId']
        let delete_gall_id = 'delete_gallery|' + value['data'][0]['galleryId']

        stmt += '<div class="card m-b-20">' +
            '<div class="card-body">' +
            '<h4 class="mt-0 header-title">' + value['galleryName'] +
            '<button type="submit" id="' + delete_gall_id + '" class="btn btn-danger ml-2 btn-sm" onClick="deleteGallery(this.id)">Delete Gallery</button>' +
            '</h4>' +
            '<hr>' +
            '<div class="row">';

        value['data'].forEach(element => {

            let delete_id = 'delete|' + element['imageId']
            stmt += '<div class="col-2 justify-content-center">' +
                '<img src="' + element['imageUrl'] + '" style="width: 100%;">' +
                '<button type="submit" id="' + delete_id + '" class="btn btn-danger btn-sm mt-2" onClick="deleteImage(this.id)"><i class="fa fa-trash" aria-hidden="true"></i></button>' +
                '</div>'
        });

        stmt += '</div>' +
            '</div>' +
            '</div>' +
            '</div>'
    });

    $("#image-gallery-container").html('')
    $("#image-gallery-container").append(stmt)
}


function deleteImage(id) {
    let delete_id = id.split('|');

    let gallery_data_for_edit;
    $.each(gallery_main_container, function (index, value) {
        if (value.imageId === delete_id[1]) {
            gallery_data_for_edit = value
        }
    })
    $('#loadingModal').modal('show');
    return $.ajax({
        type: "DELETE",
        url: "gallery/deleteImage",
        data: gallery_data_for_edit,
        dataType: "json",
        success: function (data) {
            showToastr("You successfully Delete One Image", 'Image Gallery', 'success');
            loadGalleryPage();
            setTimeout(() => {
                $('#loadingModal').modal('hide');
            }, 500);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            $('#loadingModal').modal('hide');
            showToastr("Somthing Went Wrong", 'Image Gallery', 'error');
        }
    });

}

function deleteGallery(id) {
    let delete_gallery_id = id.split('|');

    let obj = {
        galleryId: delete_gallery_id[1]
    }
    $('#loadingModal').modal('show');
    return $.ajax({
        type: "DELETE",
        url: "gallery/deleteImageGallery",
        data: obj,
        dataType: "json",
        success: function (data) {
            showToastr("You successfully Delete One Image", 'Image Gallery', 'success');
            loadGalleryPage();
            setTimeout(() => {
                $('#loadingModal').modal('hide');
            }, 500);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            $('#loadingModal').modal('hide');
            showToastr("Somthing Went Wrong", 'Image Gallery', 'error');
        }
    });

}
