
let service_container;

$(document).ready(function () {
    if ($("#service_content").length > 0) {
        tinymce.init({
            selector: "textarea#service_content",
            theme: "modern",
            height: 200,
            plugins: [
                "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                "save table contextmenu directionality emoticons template paste textcolor"
            ],
            toolbar: "insertfile undo redo | fontsizeselect | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons",


        });
    }
    loadServicePageContentPage();
});
function serviceImageChange(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#service_img_uri').attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}
function getServiceContent() {
    return $.ajax({
        url: "service/getServiceDetails",
        dataType: "json",
    });
}

function loadServicePageContentPage() {
    // $('#loadingModal').modal('show');
    $.when(
        getServiceContent()
    )
        .done(function (p1) {
            loadServiceContentView(p1);
            setTimeout(() => {
                $('#loadingModal').modal('hide');
            }, 500);
        }).fail(function (xhr) {
            $('#loadingModal').modal('hide');
            if (!isDefaultHandledAjaxError(xhr)) {
                const msg = 'Unexpected Server Exception. ';
                showToastr(msg, 'Page Loading Failed', 'error');
            }
        });
}
function loadServiceContentView(data) {
    service_container = data['Payload'];
    let stmt = '';
    $.each(data['Payload'], function (index, value) {
        let edit_id = 'edit|' + value['id']
        let delete_id = 'delete|' + value['id']
        stmt += '<div class="row mt-5">' +
            '<div class="col-md-6">' +
            '<img src="' + value['serviceImageUrl'] + '" style="width:50%;height:auto"></img>' +
            '</div>' +
            '<div class="col-md-6">' +
            '<div class="mt-30">' +
            '<h2 class="header-title">' + value['serviceName'] + '</h2>' +
            '</div>' +
            '<h4 class="header-title">' + value['serviceHeading'] + '</h4>' +
            '<p class="text-muted m-b-30">' + value['serviceContent'] + '</p>' +
            '<button type="submit" id="' + edit_id + '" class="btn btn-info waves-effect waves-light mt-20 " onClick="editServiceContent(this.id)">Edit</button>' +
            '<button type="submit" id="' + edit_id + '" class="btn btn-danger waves-effect waves-light mt-20 ml-2" onClick="deleteServiceContent(this.id)">Delete</button>' +
            '</div>' +
            '</div>' +
            '</div>'

    })

    $('#serviceDetails').html('')
    $('#serviceDetails').append(stmt)

}

$("#service_conten_form").validate({
    submitHandler: function (form) {
        var form_btn = $(form).find('button[type="submit"]');
        var form_result_div = '#form-result';
        $(form_result_div).remove();
        form_btn.before(
            '<div id="form-result" class="alert alert-success" role="alert" style="display: none;"></div>'
        );

        var service_info_form = document.getElementById('service_conten_form')
        var service_image = document.getElementById("service_image").files[0];
        var myContent = tinymce.get("service_content").getContent();
        let serviceBoby = {};
        serviceBoby['serviceContent'] = myContent,
            serviceBoby['serviceHeading'] = service_info_form.service_heading.value,
            serviceBoby['serviceName'] = service_info_form.service_name.value,
            serviceBoby['id'] = service_info_form.id.value

        if (service_container.length != 0) {
            $.each(service_container, function (index, value) {
                if (value.id === service_info_form.id.value) {
                    serviceBoby['serviceImageName'] = value['serviceImageName'];
                    serviceBoby['serviceImageUrl'] = value['serviceImageUrl'];
                }
            })
        }

        let formData = new FormData();
        formData.append("serviceImage", service_image);
        formData.append("serviceBody", JSON.stringify(serviceBoby));
        postServiceContent(formData)
        document.getElementById("service_conten_form").reset();
        $('#service_img_uri').attr('src', 'public/assets/images/upload.jpg');
    }
});


function postServiceContent(data) {
    $('#loadingModal').modal('show');
    return $.ajax({
        type: "POST",
        url: "service/setServiceDetails",
        data: data,
        dataType: "json",
        processData: false,
        contentType: false,
        success: function (data) {
            showToastr("Successfully Add Service Content", 'Service Page', 'success');
            loadServicePageContentPage();
            setTimeout(() => {
                $('#loadingModal').modal('hide');
            }, 500);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            $('#loadingModal').modal('hide');
            showToastr("Somthing Went Wrong", 'Service Page', 'error');
        }
    });
}

function editServiceContent(id) {
    edit_id = id.split('|');
    let service_data_for_edit;
    $.each(service_container, function (index, value) {
        if (value.id === edit_id[1]) {
            service_data_for_edit = value
        }
    })
    document.getElementById('service_conten_form').reset();
    var serviceFormData = document.getElementById('service_conten_form')
    serviceFormData.service_heading.value = service_data_for_edit.serviceHeading
    tinymce.get("service_content").setContent(service_data_for_edit.serviceContent);
    serviceFormData.service_name.value = service_data_for_edit.serviceName
    serviceFormData.id.value = service_data_for_edit.id
    fetch(service_data_for_edit.serviceImageUrl)
        .then(async response => {
            const contentType = response.headers.get('content-type')
            const blob = await response.blob()
            const file = new File([blob], service_data_for_edit.serviceImageName, { contentType })

            let fileInputElement = document.getElementById('service_image');
            let container = new DataTransfer();
            container.items.add(file);
            fileInputElement.files = container.files;
            serviceImageChange(fileInputElement)
        })

    $('html, body').animate({
        scrollTop: $("#service_editor").offset().top
    }, 1000);

}

function deleteServiceContent(id) {

    edit_id = id.split('|');
    let service_data_for_edit;
    $.each(service_container, function (index, value) {
        if (value.id === edit_id[1]) {
            service_data_for_edit = value
        }
    })
    $('#loadingModal').modal('show')
    return $.ajax({
        type: "DELETE",
        url: "service/deleteService",
        data: service_data_for_edit,
        dataType: "json",
        success: function (data) {
            showToastr("Successfully Deleted Service Content", 'Service Page', 'success');
            loadServicePageContentPage();
            setTimeout(() => {
                $('#loadingModal').modal('hide');
            }, 500);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            $('#loadingModal').modal('hide');
            showToastr("Somthing Went Wrong", 'Service Page', 'error');
        }
    });

}
