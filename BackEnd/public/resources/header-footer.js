let footer_container;
let header_container;


$(document).ready(function () {
    $('form').parsley();
    $('#submit_hader_logo').hide();
    $('#submit_hader_logo_transparent').hide();
    $('#submit_footer').hide();
    $('#submit_header').hide();
    loadHeaderFooterPageContent();
});

function getFooterContantDetails() {
    return $.ajax({
        url: "headerfooter/getHeaderFooterData",
        dataType: "json",
    });
}

function loadHeaderFooterPageContent() {
    // $('#loadingModal').modal('show');
    $.when(
        getFooterContantDetails()
    )
        .done(function (p1) {

            loadHeaderView(p1);
            loadFooterView(p1);
            // setTimeout(() => {
            //     $('#loadingModal').modal('hide');
            // }, 500);

        }).fail(function (xhr) {
            $('#loadingModal').modal('hide');
            if (!isDefaultHandledAjaxError(xhr)) {
                const msg = 'Unexpected Server Exception. ';
                showToastr(msg, 'Page Loading Failed', 'error');
            }
        });
}

function loadHeaderView(data) {
    header_container = data['Payload'][0];
    let stmt = '';
    stmt += '<img src="' + header_container['header_logo_uri'] + '" style="width=70%; height=auto">' +
        ' <h4 class="mt-30 header-title">' + header_container['header_contact'] + '</h4>' +
        ' <h4 class="mt-30 header-title">' + header_container['header_time'] + '</h4>';

    $('#headerDetails').html('')
    $('#headerDetails').append(stmt)

}

function loadFooterView(data) {
    footer_container = data['Payload'][0];
    let stmt = '';
    stmt += '<img src="' + footer_container['footer_logo_uri'] + '" style="width=70%; height=auto">' +
        ' <h4 class="mt-30 header-title">' + footer_container['footer_coppyright'] + '</h4>' +
        ' <h4 class="mt-30 header-title">' + footer_container['footer_content'] + '</h4>' +
        ' <h4 class="mt-30 header-title">' + footer_container['footer_linkedin'] + '</h4>' +
        ' <h4 class="mt-30 header-title">' + footer_container['footer_insta'] + '</h4>' +
        ' <h4 class="mt-30 header-title">' + footer_container['footer_facebook'] + '</h4>' +
        ' <h4 class="mt-30 header-title">' + footer_container['footer_twetter'] + '</h4>' +
        ' <h4 class="mt-30 header-title">' + footer_container['footer_youtube'] + '</h4>'

    $('#fooetrDetails').html('')
    $('#fooetrDetails').append(stmt)

}


function headerLogoChange(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#header_logo_url').attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}


function footerLogoChange(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#footer_logo_uri').attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}


function postFooterContent(data) {
    // $('#loadingModal').modal('show');
    return $.ajax({
        type: "POST",
        url: "headerfooter/addFooterContent",
        data: data,
        dataType: "json",
        processData: false,
        contentType: false,
        success: function (data) {
            showToastr("You successfully Add Footer Content", 'Header And Footer', 'success');
            loadHeaderFooterPageContent();
            // setTimeout(() => {
            //     $('#loadingModal').modal('hide');
            // }, 500);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            $('#loadingModal').modal('hide');
            showToastr("Somthing Went Wrong", 'Header And Footer', 'error');
        }
    });
}
function postHeaderContent(data) {
    // $('#loadingModal').modal('show');
    return $.ajax({
        type: "POST",
        url: "headerfooter/addHeaderContent",
        data: data,
        dataType: "json",
        processData: false,
        contentType: false,
        success: function (data) {
            showToastr("You successfully Add Header Content", 'Header And Footer', 'success');
            loadHeaderFooterPageContent();
            // setTimeout(() => {
            //     $('#loadingModal').modal('hide');
            // }, 500);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            $('#loadingModal').modal('hide');
            showToastr("Somthing Went Wrong", 'Header And Footer', 'error');
        }
    });
}


$('#submit_header').on('click', function (e) {
    e.preventDefault();
    var header_info_form = document.getElementById('header_info_form')
    var header_logo = document.getElementById("header_logo").files[0];
    var headerBody = {
        header_contact: header_info_form.header_contact.value,
        header_time: header_info_form.header_timing.value
    }
    if (header_container['header_logo']) {
        headerBody['header_logo'] = header_container.header_logo;
        headerBody['header_logo_uri'] = header_container.header_logo_uri;
    }

    let formData = new FormData();
    formData.append("headerLogoImage", header_logo);
    formData.append("headerBody", JSON.stringify(headerBody));
    postHeaderContent(formData)
    document.getElementById("header_info_form").reset();
    $('#header_logo_uri').attr('src', 'public/assets/images/upload.jpg');
    $('#submit_header').hide();
    $('#edit_header_content').show();

})


$('#edit_header_content').on('click', function (e) {
    var header_info_form = document.getElementById('header_info_form')
    header_info_form.header_contact.value = header_container.header_contact;
    header_info_form.header_timing.value = header_container.header_time;
    $('#header_logo_uri').attr('src', header_container['header_logo_uri']);
    $('#submit_header').show();
    $('#edit_header_content').hide()

})

$('#submit_footer').on('click', function (e) {
    e.preventDefault();
    var footer_info_form = document.getElementById('footer_info_form')
    var footer_logo = document.getElementById("footer_logo").files[0];
    var footerBody = {
        footer_content: footer_info_form.footer_content.value,
        footer_insta: footer_info_form.footer_insta_Id.value,
        footer_facebook: footer_info_form.footer_fburl.value,
        footer_twetter: footer_info_form.footer_twitter.value,
        footer_youtube: footer_info_form.footer_youtube.value,
        footer_linkedin: footer_info_form.footer_linkedin.value,
        footer_coppyright: footer_info_form.footer_copy_right.value
    }
    if (footer_container['footerLogoUrl']) {
        footerBody['footer_logo'] = footer_container.footer_logo;
        footerBody['footer_logo_uri'] = footer_container.footer_logo_uri;
    }
    let formData = new FormData();
    formData.append("footerLogoImage", footer_logo);
    formData.append("footerBody", JSON.stringify(footerBody));
    postFooterContent(formData)
    document.getElementById("footer_info_form").reset();
    $('#footer_logo_uri').attr('src', 'public/assets/images/upload.jpg');
    $('#submit_footer').hide();
    $('#edit_footer_content').show();

})


$('#edit_footer_content').on('click', function (e) {
    var footer_info_form = document.getElementById('footer_info_form')
    footer_info_form.footer_content.value = footer_container['footer_content'];
    footer_info_form.footer_insta_Id.value = footer_container['footer_insta'];
    footer_info_form.footer_fburl.value = footer_container['footer_facebook'];
    footer_info_form.footer_twitter.value = footer_container['footer_twetter'];
    footer_info_form.footer_youtube.value = footer_container['footer_youtube'];
    footer_info_form.footer_linkedin.value = footer_container['footer_linkedin'];
    footer_info_form.footer_copy_right.value = footer_container['footer_coppyright'];
    $('#footer_logo_uri').attr('src', footer_container['footer_logo_uri']);
    $('#submit_footer').show();
    $('#edit_footer_content').hide()

})

