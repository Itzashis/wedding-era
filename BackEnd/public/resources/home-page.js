
var other_content_container;
var about_us_container;
var banner_container;
var widget_container;

$(document).ready(() => {

    loadHomePage();
    $('#submit_home_others').show();
    $('#about-us-data').hide();

    // showToastr("Hi toarter", 'Page Loading Failed', 'success');
})
function getDataBannerDetails() {
    return $.ajax({
        url: "home/getBannerImages",
        dataType: "json",
    });
}
function getAboutUsData() {
    return $.ajax({
        url: "home/getAboutUs",
        dataType: "json",
    });
}
function getOtherContent() {
    return $.ajax({
        url: "home/getOtherContent",
        dataType: "json",
    });
}
function getHomeWidgetContent() {
    return $.ajax({
        url: "home/getShortWidget",
        dataType: "json",
    });
}
function loadHomePage() {
    // $('#loadingModal').modal('show');
    $.when(
        getDataBannerDetails(),
        getOtherContent(),
        getHomeWidgetContent()
    )
        .done(function (p1, p2, p3) {

            loadBannerView(p1[0]);
            loadOtherContent(p2[0])
            loadHomeWidgetContent(p3[0])
            setTimeout(() => {
                $('#loadingModal').modal('hide');
            }, 500);

        }).fail(function (xhr) {
            $('#loadingModal').modal('hide');
            if (!isDefaultHandledAjaxError(xhr)) {
                var msg = 'Unexpected Server Exception. Please contact System Admin ';
                showToastr(msg, 'Fraud Detections Trend', 'error');
            }
        });
}

function loadBannerView(data) {
    if (data['code'] == 200) {
        let stmt = ''
        banner_container = data['Payload']
        $.each(data['Payload'], function (index, value) {
            let edit_id = 'edit|' + value['id']
            let delete_id = 'delete|' + value['id']
            stmt += '<div class="row border-bottom">' +
                '<div class="col-md-3">' +
                '<img src = "' + value['bannerImageUrl'] + '" style="width:110px; height:auto">' +
                '</div>' +
                '<div class="col-md-2">' +
                '<p>' + value['bannerHeading'] + '</p>' +
                '</div>' +
                '<div class="col-md-4">' +
                '<p>' + value['bannerSubHeading'] + '</p>' +
                '</div>' +
                '<div class="col-md-3">' +
                '<button type="submit" id="' + edit_id + '" class="btn btn-info waves-effect waves-light mt-3" style="min-width:65px" onClick="editBanner(this.id)">Edit</button>' +
                '<button type="submit" id="' + delete_id + '" class="btn btn-danger waves-effect waves-light mt-1" style="min-width:65px" onClick="deleteBanner(this.id)">Delete</button>' +
                '</div>' +
                '</div>'
        });
        $('#bannerDetails').html('');
        $('#bannerDetails').append(stmt);
    }
}


function loadAboutView(data) {
    if (data['code'] == 200) {
        about_us_container = data['Payload']
        let stmt = ''
        $.each(data['Payload'], function (index, value) {
            stmt += '<div class="row">' +
                '<img src="' + value['aboutImagePath'] + '" style="width:32%; height:auto">' +
                '<div class="col-md-12">' +
                '<h3>' + value['heading'] + '</h3>' +
                '</div>' +
                '<div class="col-md-12">' +
                '<p>' + value['aboutShortContent'] + '</p>' +
                '</div>' +
                '</div>'
        });
        $('#aboutDetails').html('');
        $('#aboutDetails').append(stmt);
    }
}

function loadOtherContent(data) {
    if (data['code'] == 200) {
        let stmt = ''
        other_content_container = data['Payload']
        $.each(data['Payload'], function (index, value) {
            stmt += '<div class="row">' +
                '<div class="col-md-12">' +
                '<h3>' + value['whyChooseUsHeading'] + '</h3>' +
                '</div>' +
                '<div class="col-md-12">' +
                '<p>' + value['whyChooseUsContent'] + '</p>' +
                '</div>' +
                '<hr>' +
                '<div class="col-md-12">' +
                '<h3>' + value['serviceHeading'] + '</h3>' +
                '</div>' +
                '<div class="col-md-12">' +
                '<p>' + value['serviceContent'] + '</p>' +
                '</div>' +
                '<hr>' +
                '<div class="col-md-12">' +
                '<h3>' + value['eventHeading'] + '</h3>' +
                '</div>' +
                '<div class="col-md-12">' +
                '<p>' + value['eventContent'] + '</p>' +
                '</div>' +
                '<hr>' +
                '<div class="col-md-12">' +
                '<h3>' + value['galleryHeading'] + '</h3>' +
                '</div>' +
                '<div class="col-md-12">' +
                '<p>' + value['galleryContent'] + '</p>' +
                '</div>' +
                '<hr>' +
                '<div class="col-md-12">' +
                '<h3>' + value['teamHeading'] + '</h3>' +
                '</div>' +
                '<div class="col-md-12">' +
                '<p>' + value['teamContent'] + '</p>' +
                '</div>' +
                '<hr>' +
                '<div class="col-md-12">' +
                '<h3>' + value['contactHeading'] + '</h3>' +
                '</div>' +
                '<div class="col-md-12">' +
                '<p>' + value['contactContent'] + '</p>' +
                '</div>' +
                '<hr>' +
                '</div>'
        });
        $('#other_content').html('');
        $('#other_content').append(stmt);
    }
}


function loadHomeWidgetContent(data) {

    if (data['code'] == 200) {
        widget_container = data['Payload']
        let stmt = ''
        $.each(data['Payload'], function (index, value) {
            let edit_id = 'edit|' + value['id']
            stmt += '<div class="col-sm-4 col-md-4 text-center mb-20">' +
                '<div class="icon-box-new bg-white text-center clearfix m-0 mb-40">' +
                '<h6 class="letter-space-1 text-warning line-height-1">' + value['widgetNumber'] + '</h6>' +
                '<h6 class="letter-space-1 line-height-1">' + value['widgetHeading'] + '</h6>' +
                '<p class="letter-space-1 line-height-1">' + value['widgetContent'] + '</p>' +
                '</a>' +
                '</div>' +
                '<button type="submit" id="' + edit_id + '" class="btn btn-info waves-effect waves-light" onClick="editWidgetContent(this.id)">Edit</button>' +
                '</div>'
        });
        $('#home_widget_data').html('');
        $('#home_widget_data').append(stmt);
    }
}



function getBannerDetails(data) {
    $('#loadingModal').modal('show');
    return $.ajax({
        type: "POST",
        url: "home/setBannerImage",
        data: data,
        dataType: "json",
        processData: false,
        contentType: false,
        success: function (msg) {
            showToastr("You successfully Add one Banner", 'Banner Submission', 'success');
            loadHomePage();
            document.getElementById("bannerFrom").reset();
            $('#banner_img_data').attr('src', 'public/assets/images/upload.jpg');
            setTimeout(() => {
                $('#loadingModal').modal('hide');
            }, 500);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            // $('#loadingModal').modal('hide');
            showToastr("Somthing Went Wrong", 'Banner Submission Failed', 'error');
        }
    });
}

function getHomeOthertDetails(data) {
    $('#loadingModal').modal('show');

    return $.ajax({
        type: "POST",
        url: "home/setHomeOtherContent",
        data: data,
        dataType: "json",
        success: function (msg) {
            showToastr("You successfully Add Other Content", 'Other Content', 'success');
            loadHomePage();
            setTimeout(() => {
                $('#loadingModal').modal('hide');
            }, 500);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            $('#loadingModal').modal('hide');
            showToastr("Somthing Went Wrong", 'Other Content', 'error');
        }
    });
}

function getWidgetDetails(data) {
    $('#loadingModal').modal('show');

    return $.ajax({
        type: "POST",
        url: "home/setHomeWidget",
        data: data,
        dataType: "json",
        success: function (msg) {
            showToastr("You successfully Add Icon", 'Widget Icon', 'success');
            loadHomePage();
            setTimeout(() => {
                $('#loadingModal').modal('hide');
            }, 500);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            $('#loadingModal').modal('hide');
            showToastr("Somthing Went Wrong", 'Widget Icon', 'error');
        }

    });
}

$("#bannerFrom").validate({
    submitHandler: function (form) {
        var form_btn = $(form).find('button[type="submit"]');
        var form_result_div = '#form-result';
        $(form_result_div).remove();
        form_btn.before(
            '<div id="form-result" class="alert alert-success" role="alert" style="display: none;"></div>'
        );
        var form_btn_old_msg = form_btn.html();
        form_btn.html(form_btn.prop('disabled', true).data("loading-text"));

        var bannaerFormData = document.getElementById('bannerFrom')
        let banner_image = document.getElementById("bannerImage").files[0];
        let banner_obj = {};
        banner_obj['bannerHeading'] = bannaerFormData.bannerHeading.value;
        banner_obj['bannerSubHeading'] = bannaerFormData.bannerSubHeading.value;
        banner_obj['id'] = bannaerFormData.id.value;

        if (banner_container.length != 0) {
            $.each(banner_container, function (index, value) {
                if (value.id === bannaerFormData.id.value) {
                    banner_obj['bannerImageBase'] = value['bannerImageBase'];
                    banner_obj['bannerImageName'] = value['bannerImageName'];
                    banner_obj['bannerImageUrl'] = value['bannerImageUrl'];
                }
            })
        }
        let formData = new FormData();
        formData.append("bannerImage", banner_image);
        formData.append("bannerBody", JSON.stringify(banner_obj));

        getBannerDetails(formData)
        $(form_result_div).fadeOut('slow')
        form_btn.prop('disabled', false).html(form_btn_old_msg);
    }
});


$("#submit_home_others").on("click", function (e) {
    e.preventDefault();
    var home_others = document.getElementById('home_others')
    let home_other_obj = {
        'id': home_others.id.value,
        'whyChooseUsHeading': home_others.why_choose_us_heading.value,
        'whyChooseUsContent': home_others.why_choose_us_content.value,
        'serviceHeading': home_others.service_heading.value,
        'serviceContent': home_others.service_content.value,
        'eventContent': home_others.event_content.value,
        'eventHeading': home_others.event_heading.value,
        'galleryContent': home_others.gallery_content.value,
        'galleryHeading': home_others.gallery_heading.value,
        'teamHeading': home_others.team_heading.value,
        'teamContent': home_others.team_content.value,
        'contactHeading': home_others.contact_heading.value,
        'contactContent': home_others.contact_content.value,

    }

    getHomeOthertDetails(home_other_obj)
    document.getElementById("home_others").reset();
    $('#submit_home_others').hide();
    $('#edit_other_content').show();

});

$("#widget_form").validate({
    submitHandler: function (form) {
        var form_btn = $(form).find('button[type="submit"]');
        var form_result_div = '#form-result';
        $(form_result_div).remove();
        form_btn.before(
            '<div id="form-result" class="alert alert-success" role="alert" style="display: none;"></div>'
        );

        var widget = document.getElementById('widget_form')
        let widget_obj = {
            'widgetNumber': widget.widget_number.value,
            'widgetHeading': widget.widget_heading.value,
            'widgetContent': widget.Widget_content.value,
            'id': widget.id.value
        }

        getWidgetDetails(widget_obj)
        document.getElementById("widget_form").reset();
    }
});

$("#edit_other_content").on("click", function (e) {

    var home_others = document.getElementById('home_others')
    home_others.why_choose_us_heading.value = other_content_container[0]['whyChooseUsHeading']
    home_others.why_choose_us_content.value = other_content_container[0]['whyChooseUsContent']
    home_others.service_heading.value = other_content_container[0]['serviceHeading']
    home_others.service_content.value = other_content_container[0]['serviceContent']
    home_others.event_content.value = other_content_container[0]['eventContent']
    home_others.event_heading.value = other_content_container[0]['eventHeading']
    home_others.gallery_content.value = other_content_container[0]['galleryContent']
    home_others.gallery_heading.value = other_content_container[0]['galleryHeading']
    home_others.team_heading.value = other_content_container[0]['teamHeading']
    home_others.team_content.value = other_content_container[0]['teamContent']
    home_others.contact_heading.value = other_content_container[0]['contactHeading']
    home_others.contact_content.value = other_content_container[0]['contactContent']
    home_others.id.value = other_content_container[0]['id']


    $('#submit_home_others').show();
    $('#edit_other_content').hide();
})

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#about_img_data').attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}
function onBannerChange(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#banner_img_data').attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}

function editBanner(id) {
    edit_id = id.split('|');
    let banner_data_for_edit;
    $.each(banner_container, function (index, value) {
        if (value.id === edit_id[1]) {
            banner_data_for_edit = value
        }
    })
    document.getElementById('bannerFrom').reset();
    var bannaerFormData = document.getElementById('bannerFrom')
    // let banner_image = document.getElementById("bannerImage").files[0];
    bannaerFormData.bannerHeading.value = banner_data_for_edit.bannerHeading
    bannaerFormData.bannerSubHeading.value = banner_data_for_edit.bannerSubHeading
    bannaerFormData.id.value = banner_data_for_edit.id
    fetch(banner_data_for_edit.bannerImageUrl)
        .then(async response => {
            const contentType = response.headers.get('content-type')
            const blob = await response.blob()
            const file = new File([blob], banner_data_for_edit.studentImageName, { contentType })

            let fileInputElement = document.getElementById('bannerImage');
            let container = new DataTransfer();
            container.items.add(file);
            fileInputElement.files = container.files;
            onBannerChange(fileInputElement)
        })


}
function deleteBanner(id) {
    console.log(id)
    edit_id = id.split('|');
    let banner_data_for_edit;
    $.each(banner_container, function (index, value) {
        if (value.id === edit_id[1]) {
            banner_data_for_edit = value
        }
    })
    $('#loadingModal').modal('show');
    return $.ajax({
        type: "DELETE",
        url: "home/deleteBannerImage",
        data: banner_data_for_edit,
        dataType: "json",
        success: function (msg) {
            showToastr("You successfully Delete One Banner", 'Banner Section', 'success');
            loadHomePage();
            setTimeout(() => {
                $('#loadingModal').modal('hide');
            }, 500);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            // $('#loadingModal').modal('hide');
            showToastr("Somthing Went Wrong", 'Banner Section', 'error');
        }
    });

}

function editWidgetContent(id) {
    edit_id = id.split('|');
    let widget_data_for_edit;
    $.each(widget_container, function (index, value) {
        if (value.id === edit_id[1]) {
            widget_data_for_edit = value
        }
    })
    var widget = document.getElementById('widget_form')

    // let banner_image = document.getElementById("bannerImage").files[0];
    widget.widget_number.value = widget_data_for_edit.widgetNumber
    widget.widget_heading.value = widget_data_for_edit.widgetHeading
    widget.Widget_content.value = widget_data_for_edit.widgetContent
    widget.id.value = widget_data_for_edit.id
}
