
let user_container;

$(document).ready(function () {
    loadUserPageContentPage();
});

function getUsesDataFromApi() {
    return $.ajax({
        url: "user/getAllUsers",
        dataType: "json",
    });
}

function loadUserPageContentPage() {
    // $('#loadingModal').modal('show');
    $.when(
        getUsesDataFromApi()
    )
        .done(function (p1) {
            loadUserData(p1);
            setTimeout(() => {
                $('#loadingModal').modal('hide');
            }, 500);
        }).fail(function (xhr) {
            $('#loadingModal').modal('hide');
            if (!isDefaultHandledAjaxError(xhr)) {
                const msg = 'Unexpected Server Exception. ';
                showToastr(msg, 'Page Loading Failed', 'error');
            }
        });
}
function loadUserData(data) {
    user_container = data['Payload'];
    let stmt = '';
    stmt += '<table class="table">' +
        '<tr>' +
        '<th>Name</th>' +
        '<th>Email</th>' +
        '<th>Mobile</th>' +
        '<th>Message</th>' +
        '<th>Subject</th>' +
        '<th>Action</th>' +
        '</tr>' +
        '<tboby>';
    $.each(data['Payload'], function (index, value) {
        let edit_id = 'edit|' + value['id']
        let delete_id = 'delete|' + value['id']
        stmt += '<tr>' +
            '<td>' + value['userFullName'] + '</td >' +
            '<td>' + value['userEmail'] + '</td>' +
            '<td>' + value['userMob'] + '</td>' +
            '<td>' + value['userMessage'] + '</td>' +
            '<td>' + value['userSubject'] + '</td>' +
            '<td>' +
            '<button type="submit" id="' + edit_id + '" class="btn btn-danger waves-effect waves-light mt-20 ml-2" onClick="deleteuserContent(this.id)">Delete</button>' +
            '</td>' +
            '</tr>';
    })

    stmt += '</tbody>' +
        '</table >';

    $('#userDetails').html('')
    $('#userDetails').append(stmt)

}


function deleteuserContent(id) {
    edit_id = id.split('|');
    let user_data_for_edit;
    $.each(user_container, function (index, value) {
        if (value.id === edit_id[1]) {
            user_data_for_edit = value
        }
    })
    // $('#loadingModal').modal('show')
    return $.ajax({
        type: "DELETE",
        url: "user/deleteUserData",
        data: user_data_for_edit,
        dataType: "json",
        success: function (data) {
            showToastr("Successfully Deleted user Content", 'user Page', 'success');
            loadUserPageContentPage();
            setTimeout(() => {
                $('#loadingModal').modal('hide');
            }, 500);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            $('#loadingModal').modal('hide');
            showToastr("Somthing Went Wrong", 'user Page', 'error');
        }
    });

}
