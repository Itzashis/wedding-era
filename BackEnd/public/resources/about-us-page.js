
let aboutUs_container;

$(document).ready(function () {
    if ($("#about_us_description").length > 0) {
        tinymce.init({
            selector: "textarea#about_us_description",
            theme: "modern",
            height: 300,
            plugins: [
                "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                "save table contextmenu directionality emoticons template paste textcolor"
            ],
            toolbar: "insertfile undo redo | fontsizeselect | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons",


        });
    }
    if ($("#more_about_us_description").length > 0) {
        tinymce.init({
            selector: "textarea#more_about_us_description",
            theme: "modern",
            height: 300,
            plugins: [
                "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                "save table contextmenu directionality emoticons template paste textcolor"
            ],
            toolbar: "insertfile undo redo | fontsizeselect | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons",


        });
    }
    $('#submit_about_us').hide();
    $('#more_about_container').hide();
    loadAboutUsIndexPage();
});

function getAboutUsData() {
    return $.ajax({
        url: "home/getAboutUs",
        dataType: "json",
    });
}

function loadAboutUsIndexPage() {
    // $('#loadingModal').modal('show');
    $.when(
        getAboutUsData()
    )
        .done(function (p1) {
            loadAboutUsView(p1);
            // setTimeout(() => {
            //     $('#loadingModal').modal('hide');
            // }, 500);

        }).fail(function (xhr) {
            $('#loadingModal').modal('hide');
            if (!isDefaultHandledAjaxError(xhr)) {
                const msg = 'Unexpected Server Exception. ';
                showToastr(msg, 'Page Loading Failed', 'error');
            }
        });
}
function loadAboutUsView(data) {
    aboutUs_container = data['Payload'][0]
    let stmt = '';
    if (aboutUs_container.aboutUsContent) {
        stmt += '<img src="' + aboutUs_container['aboutImageUrl'] + '" style="width:80%; height:auto">' +
            ' <h4 class="mt-30 header-title">' + aboutUs_container['aboutUsContent'] + '</h4>' +
            ' <h4 class="mt-30 header-title">Video Url: ' + aboutUs_container['aboutVideoUrl'] + '</h4>';

        $('#about_us').html('')
        $('#more_about_us').html('')
        $('#about_us').append(stmt)
        $('#more_about_us').append(aboutUs_container.aboutusMoreContent)

        // $('#about_us_editor').hide();
    }
}

$("#submit_about_us").on("click", function (e) {
    e.preventDefault();
    var myContent = tinymce.get("about_us_description").getContent();
    var about_info_form = document.getElementById('about_info_form')
    var about_image = document.getElementById("about_img").files[0];
    let aboutBoby = {
        aboutUsContent: myContent,
        aboutVideoUrl: about_info_form.video_url.value,
        id: aboutUs_container.id
    }
    if (aboutUs_container['aboutImageName']) {
        aboutBoby['aboutImageName'] = aboutUs_container.aboutImageName;
        aboutBoby['aboutImageUrl'] = aboutUs_container.aboutImageUrl;
    }
    let formData = new FormData();
    formData.append("aboutImage", about_image);
    formData.append("aboutUsBoby", JSON.stringify(aboutBoby));
    postAboutUs(formData)
    document.getElementById("about_info_form").reset();
    $('#submit_about_us').hide();
    $('#edit_about_us').show();
    // $('#about_us_editor').hide();
});


$("#submit_about_us_more").on("click", function (e) {
    e.preventDefault();
    var myContent = tinymce.get("more_about_us_description").getContent();
    let aboutBoby = {
        aboutusMoreContent: myContent,
        id: aboutUs_container.id
    }
    postAboutUsMoreContent(aboutBoby)

    $('#edit_about_us_more').show();
    $('#more_about_container').hide();
    // $('#about_us_editor').hide();
});


$("#edit_about_us").on("click", function (e) {
    tinymce.get("about_us_description").setContent(aboutUs_container.aboutUsContent);
    var about_info_form = document.getElementById('about_info_form')
    about_info_form.video_url.value = aboutUs_container.aboutVideoUrl;
    fetch(aboutUs_container.aboutImageUrl)
        .then(async response => {
            const contentType = response.headers.get('content-type')
            const blob = await response.blob()
            const file = new File([blob], aboutUs_container.aboutImageName, { contentType })

            let fileInputElement = document.getElementById('about_img');
            let container = new DataTransfer();
            container.items.add(file);
            fileInputElement.files = container.files;
            aboutImageChange(fileInputElement)
        })

    $('#submit_about_us').show();
    $('#edit_about_us').hide();
});

$("#edit_about_us_more").on("click", function (e) {
    tinymce.get("more_about_us_description").setContent(aboutUs_container.aboutusMoreContent);
    $('#submit_about_us').show();
    $('#edit_about_us_more').hide();
    $('#more_about_container').show();
});


function postAboutUsMoreContent(data) {
    // $('#loadingModal').modal('show');
    return $.ajax({
        type: "POST",
        url: "home/setAboutUsMoreContent",
        data: data,
        dataType: "json",
        success: function (msg) {
            showToastr("You successfully Add About us content", 'About Us', 'success');
            loadAboutUsIndexPage();
            document.getElementById("about_us_desc_contect").reset();
            setTimeout(() => {
                $('#loadingModal').modal('hide');
            }, 500);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            $('#loadingModal').modal('hide');
            showToastr("Somthing Went Wrong", 'About Us', 'error');
        }
    });
}
function postAboutUs(data) {
    // $('#loadingModal').modal('show');
    return $.ajax({
        type: "POST",
        url: "home/setAboutUs",
        data: data,
        dataType: "json",
        processData: false,
        contentType: false,
        success: function (msg) {
            showToastr("You successfully Add About us content", 'About Us', 'success');
            loadAboutUsIndexPage();
            document.getElementById("about_us_desc_contect").reset();
            setTimeout(() => {
                $('#loadingModal').modal('hide');
            }, 500);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            $('#loadingModal').modal('hide');
            showToastr("Somthing Went Wrong", 'About Us', 'error');
        }
    });
}


function aboutImageChange(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#about_img_uri').attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}
