let contact_container;

$(document).ready(function () {
    $('form').parsley();
    $('#submit_contact_form').hide();
    loadContactPageContent();
});

function getContactApiDetails() {
    return $.ajax({
        url: "contact/getContactData",
        dataType: "json",
    });
}

function loadContactPageContent() {
    // $('#loadingModal').modal('show');
    $.when(
        getContactApiDetails()
    )
        .done(function (p1) {
            loadContactView(p1);
            // setTimeout(() => {
            //     $('#loadingModal').modal('hide');
            // }, 500);

        }).fail(function (xhr) {
            $('#loadingModal').modal('hide');
            if (!isDefaultHandledAjaxError(xhr)) {
                const msg = 'Unexpected Server Exception. ';
                showToastr(msg, 'Page Loading Failed', 'error');
            }
        });
}


function loadContactView(data) {
    contact_container = data['Payload'][0];
    let stmt = '';
    stmt += ' <h4 class="mt-30 header-title">' + contact_container['contactMap'] + '</h4>' +
        ' <h4 class="mt-30 header-title">Time: ' + contact_container['contactHour'] + '</h4>' +
        ' <h4 class="mt-30 header-title">Address: ' + contact_container['contactAddress'] + '</h4>' +
        ' <h4 class="mt-30 header-title">Email: ' + contact_container['conatctEmail'] + '</h4>' +
        ' <h4 class="mt-30 header-title">Contact Number: ' + contact_container['contcatNumber'] + '</h4>' +
        ' <h4 class="mt-30 header-title">Face Book Id' + contact_container['contactFbId'] + '</h4>' +
        ' <h4 class="mt-30 header-title">Whatsapp Number:' + contact_container['contactWpNumber'] + '</h4>' +
        ' <h4 class="mt-30 header-title">Whatsapp Intro: ' + contact_container['contactWpIntro'] + '</h4>'

    $('#contactDetails').html('')
    $('#contactDetails').append(stmt)

}

function postFooterContent(data) {
    // $('#loadingModal').modal('show');
    return $.ajax({
        type: "POST",
        url: "contact/addContcatDetails",
        data: data,
        dataType: "json",
        success: function (data) {
            showToastr("You successfully Add Footer Content", 'Header And Footer', 'success');
            loadContactPageContent();
            // setTimeout(() => {
            //     $('#loadingModal').modal('hide');
            // }, 500);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            $('#loadingModal').modal('hide');
            showToastr("Somthing Went Wrong", 'Header And Footer', 'error');
        }
    });
}

$('#submit_contact_form').on('click', function (e) {
    e.preventDefault();
    var contact_info_form = document.getElementById('contact_info_form')
    var contactBody = {
        contactMap: contact_info_form.contact_map.value,
        contactAddress: contact_info_form.contact_address.value,
        contactHour: contact_info_form.contact_hour.value,
        conatctEmail: contact_info_form.conatct_email.value,
        contcatNumber: contact_info_form.contcat_number.value,
        contactFbId: contact_info_form.contact_fb_id.value,
        contactWpNumber: contact_info_form.contact_wp_number.value,
        contactWpIntro: contact_info_form.contact_wp_intro.value,
        id: contact_container['id']
    }
    postFooterContent(contactBody)
    document.getElementById("contact_info_form").reset();

    $('#submit_contact_form').hide();
    $('#edit_contact_content').show();

})


$('#edit_contact_content').on('click', function (e) {
    var contact_info_form = document.getElementById('contact_info_form')
    contact_info_form.contact_map.value = contact_container['contactMap'];
    contact_info_form.contact_address.value = contact_container['contactAddress'];
    contact_info_form.contact_hour.value = contact_container['contactHour'];
    contact_info_form.conatct_email.value = contact_container['conatctEmail'];
    contact_info_form.contcat_number.value = contact_container['contcatNumber'];
    contact_info_form.contact_fb_id.value = contact_container['contactFbId'];
    contact_info_form.contact_wp_number.value = contact_container['contactWpNumber'];
    contact_info_form.contact_wp_intro.value = contact_container['contactWpIntro'];
    $('#submit_contact_form').show();
    $('#edit_contact_content').hide()

})

