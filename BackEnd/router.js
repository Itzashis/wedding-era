var express = require('express');
const router = express.Router();
// router.use(() => { });
// Dashboard

router.get('/dashboard', (req, res, next) => {
    res.locals = { title: 'Dashboard' };
    res.render('Dashboard/dashboard')
})

//admin pages
router.get('/home-page', (req, res) => {
    res.locals = { title: 'Home' };
    res.render('admin/home-page');
})
router.get('/aboutus-page', (req, res) => {
    res.locals = { title: 'About Us Editor' };
    res.render('admin/aboutus-page');
})
router.get('/service-page', (req, res) => {
    res.locals = { title: 'Service Editor' };
    res.render('admin/service-page');
})
router.get('/header-footer', (req, res) => {
    res.locals = { title: 'Header & Footer' };
    res.render('admin/headar-footer');
})
router.get('/team', (req, res) => {
    res.locals = { title: 'Our Team' };
    res.render('admin/team');
})
router.get('/contact', (req, res) => {
    res.locals = { title: 'Contact Us' };
    res.render('admin/contact');
})
router.get('/image-gallery', (req, res) => {
    res.locals = { title: 'Image' };
    res.render('admin/imageGallery');
})
router.get('/testimonial', (req, res) => {
    res.locals = { title: 'Testimonials' };
    res.render('admin/testimonial');
})
router.get('/user', (req, res) => {
    res.locals = { title: 'User and Contacts' };
    res.render('admin/all-user');
})
router.get('/video-gallery', (req, res) => {
    res.locals = { title: 'Video Gallery' };
    res.render('admin/video-gallery');
})



module.exports = router;