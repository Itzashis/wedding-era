const express = require('express');
const router = express.Router();
const multer = require('multer');
const db_con = require('../connection')
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'uploads/')
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + file.originalname)
    }
})

let upload = multer({
    storage: storage,

})

router.get('/', (req, res, next) => {
    res.render('index')
})
// { "categoryName": "test", "subCategoryName": "", "featureCategoryName": "" }

router.post('/addMember', upload.single('memberImage'), (req, res) => {
    const memberBody = JSON.parse(req.body.memberBody)
    if (memberBody.id.length != 0) {
        let body = {}
        if (req.file) {
            body['memberImageName'] = req.file.filename;
            body['memberImageUrl'] = process.env.BASE_URI + req.file.path;
            body['memberImageBase'] = req.file.path;
        } else {
            body['memberImageName'] = memberBody.memberImageName;
            body['memberImageUrl'] = memberBody.memberImageUrl;
            body['memberImageBase'] = memberBody.memberImageBase;
        }
        body['memberName'] = memberBody.memberName;
        body['memberPost'] = memberBody.memberPost;
        var query = `UPDATE member_details SET ? WHERE id= ?`;

        db_con.query(query, [body, memberBody.id], (error, results, fields) => {
            if (error) {
                console.log(error)
                res.send({
                    "code": 400,
                    "failed": "error request"
                })
            } else {
                res.send({
                    "code": 200,
                    "success": "Success"
                });
            }
        });
    } else {

        let memberImageName = req.file.filename;
        let memberImageUrl = process.env.BASE_URI + req.file.path;
        let memberImageBase = req.file.path;
        let memberName = memberBody.memberName;
        let memberPost = memberBody.memberPost;
        let post_id = id(8).toString();
        let query = 'INSERT INTO member_details (id,memberImageName, memberImageUrl,memberImageBase,memberName,memberPost) VALUES (?,?,?,?,?,?);';

        db_con.query(query, [post_id, memberImageName,
            memberImageUrl, memberImageBase, memberName, memberPost], (error, results, fields) => {
                if (error) {
                    console.log("error ocurred", error);
                    res.send({
                        "code": 400,
                        "failed": "error request"
                    })
                } else {
                    console.log('Resultats: ', results);
                    res.send({
                        "code": 200,
                        "success": "Success"
                    });
                }
            });
    }
})


const id = (length) => {
    let mask = ''
    mask += '0123456789';
    let result = ''
    for (var i = length; i > 0; --i) result += mask[Math.round(Math.random() * (mask.length - 1))];
    return result;
}

router.get('/getMemberData', (req, res) => {
    // query = "SELECT * FROM student_details id DESC"
    query = "SELECT * FROM member_details ORDER BY id LIMIT 0,3"
    db_con.query(query, function (error, results, fields) {
        if (error) {
            console.log(error)
            res.send({
                "code": 400,
                "failed": "error request"
            })
        } else {
            res.send({
                "code": 200,
                "success": "Success",
                "Payload": results
            });
        }
    });
})


// delete api

router.delete('/deleteMember', (req, res) => {
    const body = req.body
    let query = `DELETE FROM student_details WHERE id = ?`;
    db_con.query(query, [body.id], (error, results, fields) => {
        if (error) {
            console.log("error ocurred", error);
            res.send({
                "code": 400,
                "failed": "error request"
            })
        } else {
            console.log('Resultats: ', results);
            res.send({
                "code": 200,
                "success": "Success"
            });
        }
    });

})


module.exports = router;