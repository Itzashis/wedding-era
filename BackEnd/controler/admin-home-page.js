const express = require('express');
const router = express.Router();
const multer = require('multer');
const db_con = require('../connection')

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'uploads/')
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + file.originalname)
    }
})

let upload = multer({
    storage: storage,

})

router.get('/', (req, res, next) => {
    res.render('index')
})
// { "categoryName": "test", "subCategoryName": "", "featureCategoryName": "" }

router.post('/setBannerImage', upload.single('bannerImage'), (req, res) => {

    const bannerBody = JSON.parse(req.body.bannerBody)
    if (bannerBody.id.length != 0) {
        let body = {}
        if (req.file) {
            body['bannerImageName'] = req.file.filename;
            body['bannerImageUrl'] = process.env.BASE_URI + req.file.path;
            body['bannerImageBase'] = req.file.path;
        } else {
            body['bannerImageName'] = bannerBody.bannerImageName;
            body['bannerImageUrl'] = bannerBody.bannerImageUrl;
            body['bannerImageBase'] = bannerBody.bannerImageBase;
        }
        body['bannerHeading'] = bannerBody.bannerHeading;
        body['bannerSubHeading'] = bannerBody.bannerSubHeading;
        var query = `UPDATE home_banner SET ? WHERE id= ?`;
        db_con.query(query, [body, bannerBody.id], (error, results, fields) => {
            if (error) {
                res.send({
                    "code": 400,
                    "failed": "error request"
                })
            } else {
                res.send({
                    "code": 200,
                    "success": "Success"
                });
            }
        });
    } else {
        let bannerImageName = req.file.filename;
        let bannerImageUrl = process.env.BASE_URI + req.file.path;
        let bannerImageBase = req.file.path;
        let bannerHeading = bannerBody.bannerHeading;
        let bannerSubHeading = bannerBody.bannerSubHeading;
        let post_id = id(8).toString();

        let query = 'INSERT INTO home_banner (id,bannerImageName, bannerImageUrl,bannerHeading,bannerSubHeading,bannerImageBase) VALUES (?,?,?,?,?,?);';

        db_con.query(query, [post_id, bannerImageName,
            bannerImageUrl, bannerHeading, bannerSubHeading, bannerImageBase], (error, results, fields) => {
                if (error) {
                    res.send({
                        "code": 400,
                        "failed": "error request"
                    })
                } else {
                    res.send({
                        "code": 200,
                        "success": "Success"
                    });
                }
            });
    }
})


router.post('/setHomeOtherContent', (req, res) => {
    const body = req.body
    if (body.id.length != 0) {
        var query = `UPDATE other_content SET ? WHERE id= ?`;
        db_con.query(query, [body, body.id], (error, results, fields) => {
            if (error) {
                res.send({
                    "code": 400,
                    "failed": "error request"
                })
            } else {
                res.send({
                    "code": 200,
                    "success": "Success"
                });
            }
        });
    } else {
        let post_id = id(8).toString();
        let query = `INSERT INTO other_content (id,whyChooseUsHeading,whyChooseUsContent,serviceHeading,serviceContent,
            eventContent,eventHeading,galleryContent,galleryHeading,teamHeading,teamContent,contactHeading,contactContent) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?);`;
        db_con.query(query, [
            post_id,
            body.whyChooseUsHeading,
            body.whyChooseUsContent,
            body.serviceHeading,
            body.serviceContent,
            body.eventContent,
            body.eventHeading,
            body.galleryContent,
            body.galleryHeading,
            body.teamHeading,
            body.teamContent,
            body.contactHeading,
            body.contactContent
        ], (error, results, fields) => {
            if (error) {
                res.send({
                    "code": 400,
                    "failed": "error request"
                })
            } else {
                res.send({
                    "code": 200,
                    "success": "Success"
                });
            }
        });
    }

})


router.post('/setAboutUs', upload.single('aboutImage'), (req, res) => {

    let query = `UPDATE about_us SET ? WHERE id= ?`;
    let aboutBody = JSON.parse(req.body.aboutUsBoby);
    if (req.file) {
        aboutBody['aboutImageName'] = req.file.filename;
        aboutBody['aboutImageUrl'] = process.env.BASE_URI + req.file.path;
    } else {
        aboutBody['aboutImageName'] = aboutBody.aboutImageName;
        aboutBody['aboutImageUrl'] = aboutBody.aboutImageUrl;
    }

    console.log(aboutBody)

    db_con.query(query, [aboutBody, aboutBody.id], (error, results, fields) => {
        if (error) {
            console.log("error ocurred", error);
            res.send({
                "code": 400,
                "failed": "error request"
            })
        } else {
            console.log('Resultats: ', results);
            res.send({
                "code": 200,
                "success": "Success"
            });
        }
    });
})

router.post('/setAboutUsMoreContent', (req, res) => {

    let query = `UPDATE about_us SET ? WHERE id= ?`;
    let aboutBody = req.body;
    db_con.query(query, [aboutBody, aboutBody.id], (error, results, fields) => {
        if (error) {
            console.log("error ocurred", error);
            res.send({
                "code": 400,
                "failed": "error request"
            })
        } else {
            console.log('Resultats: ', results);
            res.send({
                "code": 200,
                "success": "Success"
            });
        }
    });
})

router.post('/setHomeWidget', (req, res) => {
    const body = req.body
    if (body.id.length != 0) {
        var query = `UPDATE home_widget SET ? WHERE id= ?`;
        db_con.query(query, [body, body.id], (error, results, fields) => {
            if (error) {
                res.send({
                    "code": 400,
                    "failed": "error request"
                })
            } else {
                res.send({
                    "code": 200,
                    "success": "Success"
                });
            }
        });
    } else {
        let post_id = id(8).toString();
        let query = 'INSERT INTO home_widget (id,widgetNumber,widgetHeading,widgetContent) VALUES (?,?,?,?);';
        db_con.query(query, [post_id, body.widgetNumber, body.widgetHeading, body.widgetContent], (error, results, fields) => {
            if (error) {
                res.send({
                    "code": 400,
                    "failed": "error request"
                })
            } else {
                res.send({
                    "code": 200,
                    "success": "Success"
                });
            }
        });
    }

})

const id = (length) => {
    let mask = ''
    mask += '0123456789';
    let result = ''
    for (var i = length; i > 0; --i) result += mask[Math.round(Math.random() * (mask.length - 1))];
    return result;
}

router.get('/getBannerImages', (req, res) => {
    query = "SELECT * FROM home_banner"
    db_con.query(query, function (error, results, fields) {
        if (error) {
            res.send({
                "code": 400,
                "failed": "error request"
            })
        } else {
            res.send({
                "code": 200,
                "success": "Success",
                "Payload": results
            });
        }
    });
})

router.get('/getOtherContent', (req, res) => {
    query = "SELECT * FROM other_content"
    db_con.query(query, function (error, results, fields) {
        if (error) {
            res.send({
                "code": 400,
                "failed": "error request"
            })
        } else {
            res.send({
                "code": 200,
                "success": "Success",
                "Payload": results
            });
        }
    });
})
router.get('/getShortWidget', (req, res) => {
    query = "SELECT * FROM home_widget ORDER BY id LIMIT 0,3"
    db_con.query(query, function (error, results, fields) {
        if (error) {
            res.send({
                "code": 400,
                "failed": "error request"
            })
        } else {
            res.send({
                "code": 200,
                "success": "Success",
                "Payload": results
            });
        }
    });
})

// delete api

router.delete('/deleteBannerImage', (req, res) => {
    const body = req.body
    let query = `DELETE FROM home_banner WHERE id = ?`;
    db_con.query(query, [body.id], (error, results, fields) => {
        if (error) {
            console.log("error ocurred", error);
            res.send({
                "code": 400,
                "failed": "error request"
            })
        } else {
            console.log('Resultats: ', results);
            res.send({
                "code": 200,
                "success": "Success"
            });
        }
    });

})

router.get('/getAboutUs', (req, res) => {
    query = "SELECT * FROM about_us"
    db_con.query(query, function (error, results, fields) {
        if (error) {
            res.send({
                "code": 400,
                "failed": "error request"
            })
        } else {
            res.send({
                "code": 200,
                "success": "Success",
                "Payload": results
            });
        }
    });
})


module.exports = router;


9874086508