const express = require('express');
const router = express.Router();
const multer = require('multer');
const db_con = require('../connection')

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'uploads/')
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + file.originalname)
    }
})

let upload = multer({
    storage: storage,

})

router.get('/', (req, res, next) => {
    res.render('index')
})
// { "categoryName": "test", "subCategoryName": "", "featureCategoryName": "" }


var galleryUpload = upload.fields([
    {
        name: 'galleyImage',
        maxCount: 15
    }
])

var addNewGalleryUpload = upload.fields([
    {
        name: 'addNewGalleyImage',
        maxCount: 15
    }
])

router.post('/addGallery', galleryUpload, (req, res) => {
    const galID = id(8).toString();
    const galleryBody = JSON.parse(req.body.galleryBody)
    const galimg = req.files.galleyImage
    let gallery_container = [];
    galimg.forEach(element => {
        let obj = {
            galleryName: galleryBody.galleryName,
            galleryId: galID,
            imageName: element.filename,
            imageUrl: process.env.BASE_URI + element.path,
            imageBasePath: element.path,
            imageId: id(8).toString()
        }
        gallery_container.push(obj);
    });

    let tableHeader = ['galleryName', 'galleryId', 'imageName', 'imageUrl', 'imageBasePath', 'imageId']
    let query = 'INSERT INTO image_gallery (' + tableHeader + ') VALUES ?';
    let values = gallery_container.reduce((o, a) => {
        let ini = [];
        ini.push(a.galleryName);
        ini.push(a.galleryId);
        ini.push(a.imageName);
        ini.push(a.imageUrl);
        ini.push(a.imageBasePath);
        ini.push(a.imageId);
        o.push(ini);
        return o
    }, [])

    db_con.query(query, [values], (error, results, fields) => {
        if (error) {
            console.log(error)
            res.send({
                "code": 400,
                "failed": "error request"
            })
        } else {
            res.send({
                "code": 200,
                "message": "Gallery Added"
            });
        }
    });
});


router.post('/addNewVideo', galleryUpload, (req, res) => {
    const vidID = id(8).toString();
    const videoBody = req.body
    console.log(videoBody);
    let query = 'INSERT INTO video_gallery (videoId,videoLink,videoName) VALUES (?,?,?)';
    db_con.query(query, [vidID, videoBody.videoLink, videoBody.videoName], (error, results, fields) => {
        if (error) {
            console.log(error)
            res.send({
                "code": 400,
                "failed": "error request"
            })
        } else {
            res.send({
                "code": 200,
                "message": "Video Added"
            });
        }
    });
});

router.post('/addNewImageInGallery', addNewGalleryUpload, (req, res) => {
    const galleryBody = JSON.parse(req.body.galleryBody)
    const galimg = req.files.addNewGalleyImage
    let gallery_container = [];
    galimg.forEach(element => {
        let obj = {
            galleryName: galleryBody.galleryName,
            galleryId: galleryBody.galleryId,
            imageName: element.filename,
            imageUrl: process.env.BASE_URI + element.path,
            imageBasePath: element.path,
            imageId: id(8).toString()
        }
        gallery_container.push(obj);
    });

    let tableHeader = ['galleryName', 'galleryId', 'imageName', 'imageUrl', 'imageBasePath', 'imageId']
    let query = 'INSERT INTO image_gallery (' + tableHeader + ') VALUES ?';
    let values = gallery_container.reduce((o, a) => {
        let ini = [];
        ini.push(a.galleryName);
        ini.push(a.galleryId);
        ini.push(a.imageName);
        ini.push(a.imageUrl);
        ini.push(a.imageBasePath);
        ini.push(a.imageId);
        o.push(ini);
        return o
    }, [])

    console.log(gallery_container)

    db_con.query(query, [values], (error, results, fields) => {
        if (error) {
            console.log(error)
            res.send({
                "code": 400,
                "failed": "error request"
            })
        } else {
            res.send({
                "code": 200,
                "message": "Gallery Added"
            });
        }
    });

});



const id = (length) => {
    let mask = ''
    mask += '0123456789';
    let result = ''
    for (var i = length; i > 0; --i) result += mask[Math.round(Math.random() * (mask.length - 1))];
    return result;
}


router.get('/getAllImage', (req, res) => {
    query = "SELECT * FROM image_gallery"
    db_con.query(query, function (error, results, fields) {
        if (error) {
            res.send({
                "code": 400,
                "failed": "error request"
            })
        } else {
            res.send({
                "code": 200,
                "success": "Success",
                "Payload": results
            });
        }
    });
})


router.get('/getAllVideos', (req, res) => {
    query = "SELECT * FROM video_gallery"
    db_con.query(query, function (error, results, fields) {
        if (error) {
            res.send({
                "code": 400,
                "failed": "error request"
            })
        } else {
            res.send({
                "code": 200,
                "success": "Success",
                "Payload": results
            });
        }
    });
})



router.delete('/deleteImage', (req, res) => {
    const body = req.body
    let query = `DELETE FROM image_gallery WHERE imageId = ?`;
    db_con.query(query, [body.imageId], (error, results, fields) => {
        if (error) {
            console.log("error ocurred", error);
            res.send({
                "code": 400,
                "failed": "error request"
            })
        } else {
            console.log('Resultats: ', results);
            res.send({
                "code": 200,
                "success": "Success"
            });
        }
    });

})

router.delete('/deleteImageGallery', (req, res) => {
    const body = req.body
    let query = `DELETE FROM image_gallery WHERE galleryId = ?`;
    db_con.query(query, [body.galleryId], (error, results, fields) => {
        if (error) {
            console.log("error ocurred", error);
            res.send({
                "code": 400,
                "failed": "error request"
            })
        } else {
            console.log('Resultats: ', results);
            res.send({
                "code": 200,
                "success": "Success"
            });
        }
    });

})
router.delete('/deleteVideo', (req, res) => {
    const body = req.body
    console.log(body)
    let query = `DELETE FROM video_gallery WHERE videoId = ?`;
    db_con.query(query, [body.videoId], (error, results, fields) => {
        if (error) {
            console.log("error ocurred", error);
            res.send({
                "code": 400,
                "failed": "error request"
            })
        } else {
            console.log('Resultats: ', results);
            res.send({
                "code": 200,
                "success": "Success"
            });
        }
    });

})


module.exports = router;


9874086508