const express = require('express');
const router = express.Router();
const multer = require('multer');
const db_con = require('../connection')
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'uploads/')
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + file.originalname)
    }
})

let upload = multer({
    storage: storage,

})

router.get('/', (req, res, next) => {
    res.render('index')
})
// { "categoryName": "test", "subCategoryName": "", "featureCategoryName": "" }


router.post('/addFooterContent', upload.single('footerLogoImage'), (req, res) => {
    let id = 42344255;
    let query = `UPDATE header_footer SET ? WHERE id= ?`;
    let footerBody = JSON.parse(req.body.footerBody)
    if (req.file) {
        footerBody['footer_logo'] = req.file.filename;
        footerBody['footer_logo_uri'] = process.env.BASE_URI + req.file.path;
    } else {
        footerBody['footer_logo'] = footerBody.footer_logo;
        footerBody['footer_logo_uri'] = footerBody.footerLogoUrl;
    }

    db_con.query(query, [footerBody, id], (error, results, fields) => {
        if (error) {
            console.log("error ocurred", error);
            res.send({
                "code": 400,
                "failed": "error request"
            })
        } else {
            console.log('Resultats: ', results);
            res.send({
                "code": 200,
                "success": "Success"
            });
        }
    });
})

router.post('/addHeaderContent', upload.single('headerLogoImage'), (req, res) => {
    let id = 42344255;
    let query = `UPDATE header_footer SET ? WHERE id= ?`;
    let headerBody = JSON.parse(req.body.headerBody)
    if (req.file) {
        headerBody['header_logo'] = req.file.filename;
        headerBody['header_logo_uri'] = process.env.BASE_URI + req.file.path;
    } else {
        headerBody['header_logo'] = headerBody.header_logo;
        headerBody['header_logo_uri'] = headerBody.header_logo_uri;
    }

    console.log(headerBody, id)

    db_con.query(query, [headerBody, id], (error, results, fields) => {
        if (error) {
            console.log("error ocurred", error);
            res.send({
                "code": 400,
                "failed": "error request"
            })
        } else {
            console.log('Resultats: ', results);
            res.send({
                "code": 200,
                "success": "Success"
            });
        }
    });
})


const id = (length) => {
    let mask = ''
    mask += '0123456789';
    let result = ''
    for (var i = length; i > 0; --i) result += mask[Math.round(Math.random() * (mask.length - 1))];
    return result;
}

router.get('/getHeaderFooterData', (req, res) => {
    query = "SELECT * FROM header_footer"
    db_con.query(query, function (error, results, fields) {
        if (error) {
            res.send({
                "code": 400,
                "failed": "error request"
            })
        } else {
            res.send({
                "code": 200,
                "success": "Success",
                "Payload": results
            });
        }
    });
})



module.exports = router;