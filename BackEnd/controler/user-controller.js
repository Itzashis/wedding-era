const express = require('express');
const router = express.Router();
const multer = require('multer');
const nodemailer = require('nodemailer');

const db_con = require('../connection')
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'uploads/')
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + file.originalname)
    }
})

// let transporter = nodemailer.createTransport({
//     host: 'thefirstfly.com',
//     port: 465,
//     auth: {
//         user: "support@thefirstfly.com",
//         pass: "support@2021#"
//     }
// })

// let transporter = nodemailer.createTransport("SMTP", {
//     service: "Gmail",
//     auth: {
//         user: "amiabhimoy@gmail.com",
//         pass: "amipassword"
//     }
// })



let upload = multer({
    storage: storage,

})

router.get('/', (req, res, next) => {
    res.render('index')
})
// { "categoryName": "test", "subCategoryName": "", "featureCategoryName": "" }

router.post('/addNewUser', (req, res) => {
    let userFullName, userEmail, userSubject, userMob, userMsg;
    userFullName = req.body.userFullName;
    userEmail = req.body.userEmail;
    userSubject = req.body.userSubject;
    userMob = req.body.userMob;
    userMsg = req.body.userMsg;
    let post_id = id(8).toString();


    let query = 'INSERT INTO all_users (id,userFullName,userEmail,userSubject,userMob,userMsg) VALUES (?,?,?,?,?,?)';
    db_con.query(query, [post_id, userFullName, userEmail, userSubject, userMob, userMsg], (error, results, fields) => {
        if (error) {
            console.log(error)
            res.send({
                "code": 400,
                "failed": "error request"
            })
        } else {
            // var mailOptions = {
            //     from: 'amiabhimoy@gmail.com',
            //     to: userEmail,
            //     // cc: 'info@thefirstfly.com,shiv.recruiters@gmail.com',
            //     subject: 'Hi ' + userFullName,
            //     html: `<div width="100%"><img src="https://admin.thefirstfly.com/uploads/1633850535011logo-wide-white.png"><br><hr>
            //                     <hr>
            //                     <h3>Your Application is Submited Successfully. Details As Follows</h3>
            //                     <p><strong>Name :</strong> `+ userFullName + `</p>
            //                     <p><strong>Email :</strong> `+ userEmail + `</p>
            //                     <p><strong>Address :</strong>`+ userSubject + `</p>
            //                     <p><strong>Mobile :</strong> `+ userMob + `</p>
            //                     <p><strong>Message :</strong> `+ userMsg + `</p>
            //                     <br>
            //                     <br>
            //                     <hr><hr>
            //                     <h3>We Will be Contact You As Soon As Possible</h3> 
            //                     </div>`
            // };

            // transporter.sendMail(mailOptions, function (error, info) {
            //     if (error) {
            //         console.log(error);
            //     } else {
            //         console.log('Email sent: ' + info.response);
            //     }
            // });
            res.send({
                "code": 200,
                "message": "Successfully Submited"
            });
        }
    });

})


const id = (length) => {
    let mask = ''
    mask += '0123456789';
    let result = ''
    for (var i = length; i > 0; --i) result += mask[Math.round(Math.random() * (mask.length - 1))];
    return result;
}


router.get('/getAllUsers', (req, res) => {
    query = "SELECT * FROM all_users"
    db_con.query(query, function (error, results, fields) {
        if (error) {
            res.send({
                "code": 400,
                "failed": "error request"
            })
        } else {
            res.send({
                "code": 200,
                "success": "Success",
                "Payload": results
            });
        }
    });
})

//delete

router.delete('/deleteUserData', (req, res) => {
    query = `DELETE FROM all_users WHERE id = ?`
    db_con.query(query, [req.body.id], function (error, results, fields) {
        if (error) {

            res.send({
                "code": 400,
                "failed": "error request"
            })
        } else {
            res.send({
                "code": 200,
                "success": "Success",
                "Payload": results
            });
        }
    });
})




module.exports = router;