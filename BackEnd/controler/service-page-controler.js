const express = require('express');
const router = express.Router();
const multer = require('multer');
const db_con = require('../connection')
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'uploads/')
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + file.originalname)
    }
})

let upload = multer({
    storage: storage,

})

router.get('/', (req, res, next) => {
    res.render('index')
})
// { "categoryName": "test", "subCategoryName": "", "featureCategoryName": "" }

router.post('/setServiceDetails', upload.single('serviceImage'), (req, res) => {
    const serviceBody = JSON.parse(req.body.serviceBody)
    if (serviceBody.id.length != 0) {
        let body = {}
        if (req.file) {
            body['serviceImageName'] = req.file.filename;
            body['serviceImageUrl'] = process.env.BASE_URI + req.file.path;
        } else {
            body['serviceImageName'] = serviceBody.serviceImageName;
            body['serviceImageUrl'] = serviceBody.serviceImageUrl;
        }
        body['serviceHeading'] = serviceBody.serviceHeading;
        body['serviceContent'] = serviceBody.serviceContent;
        body['serviceName'] = serviceBody.serviceName;
        var query = `UPDATE service_page SET ? WHERE id= ?`;
        console.log(body)
        db_con.query(query, [body, serviceBody.id], (error, results, fields) => {
            if (error) {
                res.send({
                    "code": 400,
                    "failed": "error request"
                })
            } else {
                res.send({
                    "code": 200,
                    "success": "Success"
                });
            }
        });
    } else {
        let serviceImageName, serviceImageUrl, serviceHeading, serviceContent, serviceName;

        serviceImageName = req.file.filename;
        serviceImageUrl = process.env.BASE_URI + req.file.path;
        serviceHeading = serviceBody.serviceHeading;
        serviceContent = serviceBody.serviceContent;
        serviceName = serviceBody.serviceName;
        let post_id = id(8).toString();
        let query = 'INSERT INTO service_page (id,serviceHeading,serviceContent,serviceImageName,serviceImageUrl,serviceName) VALUES (?,?,?,?,?,?);';
        db_con.query(query, [post_id, serviceHeading, serviceContent, serviceImageName, serviceImageUrl, serviceName], (error, results, fields) => {
            if (error) {
                res.send({
                    "code": 400,
                    "failed": "error request"
                })
            } else {

                res.send({
                    "code": 200,
                    "success": "Success"
                });
            }
        });
    }
})


const id = (length) => {
    let mask = ''
    mask += '0123456789';
    let result = ''
    for (var i = length; i > 0; --i) result += mask[Math.round(Math.random() * (mask.length - 1))];
    return result;
}

router.get('/getServiceDetails', (req, res) => {
    query = "SELECT * FROM service_page"
    db_con.query(query, function (error, results, fields) {
        if (error) {
            res.send({
                "code": 400,
                "failed": "erreur survenue"
            })
        } else {
            res.send({
                "code": 200,
                "success": "Success",
                "Payload": results
            });
        }
    });
})

// delete api

router.delete('/deleteService', (req, res) => {
    const body = req.body
    let query = `DELETE FROM service_page WHERE id = ?`;
    db_con.query(query, [body.id], (error, results, fields) => {
        if (error) {
            console.log("error ocurred", error);
            res.send({
                "code": 400,
                "failed": "error request"
            })
        } else {
            console.log('Resultats: ', results);
            res.send({
                "code": 200,
                "success": "Success"
            });
        }
    });

})

module.exports = router;