const express = require('express');
const router = express.Router();
const multer = require('multer');
const db_con = require('../connection')
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'uploads/')
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + file.originalname)
    }
})

let upload = multer({
    storage: storage,

})

router.get('/', (req, res, next) => {
    res.render('index')
})
// { "categoryName": "test", "subCategoryName": "", "featureCategoryName": "" }

router.post('/setTestimonialDetails', upload.single('testimonialImage'), (req, res) => {
    const testimonialBody = JSON.parse(req.body.testimonialBody)
    if (testimonialBody.id.length != 0) {
        let body = {}
        if (req.file) {
            body['testimonialImageName'] = req.file.filename;
            body['testimonialImageUrl'] = process.env.BASE_URI + req.file.path;
        } else {
            body['testimonialImageName'] = testimonialBody.testimonialImageName;
            body['testimonialImageUrl'] = testimonialBody.testimonialImageUrl;
        }
        body['testimonialMessage'] = testimonialBody.testimonialMessage;
        body['testimonialClientName'] = testimonialBody.testimonialClientName;
        body['testimonialClientPost'] = testimonialBody.testimonialClientPost;
        var query = `UPDATE testimonial_page SET ? WHERE id= ?`;
        console.log(body)
        db_con.query(query, [body, testimonialBody.id], (error, results, fields) => {
            if (error) {
                res.send({
                    "code": 400,
                    "failed": "error request"
                })
            } else {
                res.send({
                    "code": 200,
                    "success": "Success"
                });
            }
        });
    } else {
        let testimonialImageName, testimonialImageUrl, testimonialMessage, testimonialClientName, testimonialClientPost;

        testimonialImageName = req.file.filename;
        testimonialImageUrl = process.env.BASE_URI + req.file.path;
        testimonialMessage = testimonialBody.testimonialMessage;
        testimonialClientName = testimonialBody.testimonialClientName;
        testimonialClientPost = testimonialBody.testimonialClientPost;
        let post_id = id(8).toString();
        let query = 'INSERT INTO testimonial_page (id,testimonialImageName,testimonialImageUrl,testimonialMessage,testimonialClientName,testimonialClientPost) VALUES (?,?,?,?,?,?);';
        db_con.query(query, [post_id, testimonialImageName, testimonialImageUrl, testimonialMessage, testimonialClientName, testimonialClientPost], (error, results, fields) => {
            if (error) {
                res.send({
                    "code": 400,
                    "failed": "error request"
                })
            } else {

                res.send({
                    "code": 200,
                    "success": "Success"
                });
            }
        });
    }
})


const id = (length) => {
    let mask = ''
    mask += '0123456789';
    let result = ''
    for (var i = length; i > 0; --i) result += mask[Math.round(Math.random() * (mask.length - 1))];
    return result;
}

router.get('/getTestimonialDetails', (req, res) => {
    query = "SELECT * FROM testimonial_page"
    db_con.query(query, function (error, results, fields) {
        if (error) {
            res.send({
                "code": 400,
                "failed": "erreur survenue"
            })
        } else {
            res.send({
                "code": 200,
                "success": "Success",
                "Payload": results
            });
        }
    });
})

// delete api

router.delete('/deletTetestimonial', (req, res) => {
    const body = req.body
    let query = `DELETE FROM testimonial_page WHERE id = ?`;
    db_con.query(query, [body.id], (error, results, fields) => {
        if (error) {
            console.log("error ocurred", error);
            res.send({
                "code": 400,
                "failed": "error request"
            })
        } else {
            console.log('Resultats: ', results);
            res.send({
                "code": 200,
                "success": "Success"
            });
        }
    });

})

module.exports = router;