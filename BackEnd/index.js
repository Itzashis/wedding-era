// var app = require('express')();

var express = require('express');
const app = express();

const connection = require('./connection.js')
var path = require('path');
var http = require('http').Server(app);
var bCrypt = require('bcryptjs');
var router = require('./router.js');
var Authrouter = require('./Authrouter.js');
const bodyParser = require('body-parser')
var urlencodedParser = bodyParser.urlencoded({ extended: false });
const cors = require('cors');
app.use(bodyParser.json());
// app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ limit: "50mb", extended: true, parameterLimit: 500000 }))
// cors doc

// cors doc
app.use('/uploads', express.static('uploads'))
app.use(cors({ origin: true, credentials: true }))
app.use(function (req, res, next) {

  res.header("Access-Control-Allow-Origin", req.headers.origin);
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With, Origin, Content-Type, Accept');
  res.setHeader('Access-Control-Allow-Credentials', true);
  next();
});
// Access public folder from root
app.use('/public', express.static('public'));
app.get('/layouts/', function (req, res) {
  res.render('view');
});

// Add Authentication Route file with app
app.use('/', Authrouter);

//For set layouts of html view
var expressLayouts = require('express-ejs-layouts');
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(expressLayouts);


app.use('/', router);

const home = require('./controler/admin-home-page')
const team = require('./controler/admin-team')
const headerfooter = require('./controler/header-footer-controler')
const service = require('./controler/service-page-controler')
const user = require('./controler/user-controller')
const contact = require('./controler/contact-controller')
const gallery = require('./controler/gallery-controller')
const testimonials = require('./controler/testimonial-controller')


app.use('/home', home)
app.use('/member', team)
app.use('/headerfooter', headerfooter)
app.use('/service', service)
app.use('/user', user)
app.use('/contact', contact)
app.use('/gallery', gallery)
app.use('/testimonial', testimonials)


http.listen(4301, function () {
  console.log('listening on *:4301');
});
