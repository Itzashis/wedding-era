-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 21, 2021 at 11:17 AM
-- Server version: 8.0.17
-- PHP Version: 7.2.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `weadding`
--

-- --------------------------------------------------------

--
-- Table structure for table `about_us`
--

CREATE TABLE `about_us` (
  `id` varchar(255) NOT NULL,
  `aboutUsContent` text NOT NULL,
  `aboutVideoUrl` varchar(255) NOT NULL,
  `aboutImageUrl` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `aboutImageBase` varchar(255) NOT NULL,
  `aboutImageName` varchar(255) NOT NULL,
  `aboutusMoreContent` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `about_us`
--

INSERT INTO `about_us` (`id`, `aboutUsContent`, `aboutVideoUrl`, `aboutImageUrl`, `aboutImageBase`, `aboutImageName`, `aboutusMoreContent`) VALUES
('33466746', '<h3>Eum ipsam laborum deleniti&nbsp;<strong>velit pariatur architecto aut nihil</strong></h3>\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Duis aute irure dolor in reprehenderit</p>\n<p class=\"fst-italic\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>\n<ul>\n<li>Ullamco laboris nisi ut aliquip ex ea commodo consequat.</li>\n<li>Duis aute irure dolor in reprehenderit in voluptate velit.</li>\n<li>Ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate trideta storacalaperda mastiro dolore eu fugiat nulla pariatur.</li>\n</ul>\n<p>Ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>', 'https://www.youtube.com/watch?v=52KfTaHKWgs', 'http://localhost:4301/uploads\\164008014316272.jpg', '33466746', '164008014316272.jpg', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Duis aute irure dolor in reprehenderit</p>\n<p class=\"fst-italic\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>\n<ul>\n<li>Ullamco laboris nisi ut aliquip ex ea commodo consequat.</li>\n<li>Duis aute irure dolor in reprehenderit in voluptate velit.</li>\n<li>Ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate trideta storacalaperda mastiro dolore eu fugiat nulla pariatur.</li>\n</ul>\n<p>Ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id&nbsp;</p>');

-- --------------------------------------------------------

--
-- Table structure for table `all_users`
--

CREATE TABLE `all_users` (
  `id` varchar(255) NOT NULL,
  `userFullName` text NOT NULL,
  `userEmail` text NOT NULL,
  `userSubject` text NOT NULL,
  `userMob` varchar(1024) NOT NULL,
  `userMsg` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `all_users`
--

INSERT INTO `all_users` (`id`, `userFullName`, `userEmail`, `userSubject`, `userMob`, `userMsg`) VALUES
('10665375', 'Abhimoy Kolay', 'hi.abhimoy@gmail.com', 'czx', '+918981224073', 'zx'),
('81198654', 'asd', 'hi.abhimoy@gmail.com', 'das', '+918981224073', 'as'),
('19139881', 'Abhimoy Kolay', 'hi.abhimoy@gmail.com', 'asdas', '+918981224073', 'sdas'),
('31245411', 'asis', 'asis@gmail.com', 'hi', '7765447462', 'asdas');

-- --------------------------------------------------------

--
-- Table structure for table `contact_details`
--

CREATE TABLE `contact_details` (
  `id` text NOT NULL,
  `contactMap` text NOT NULL,
  `contactHour` varchar(255) NOT NULL,
  `contactAddress` text NOT NULL,
  `conatctEmail` varchar(255) NOT NULL,
  `contcatNumber` varchar(255) NOT NULL,
  `contactFbId` varchar(255) NOT NULL,
  `contactWpNumber` varchar(255) NOT NULL,
  `contactWpIntro` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `contact_details`
--

INSERT INTO `contact_details` (`id`, `contactMap`, `contactHour`, `contactAddress`, `conatctEmail`, `contcatNumber`, `contactFbId`, `contactWpNumber`, `contactWpIntro`) VALUES
('88677648', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d58908.76246842234!2d88.83381831681362!3d22.661331751212376!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39ff567854e8190f%3A0xf12dc8b635ba902f!2sBasirhat%2C%20West%20Bengal!5e0!3m2!1sen!2sin!4v1639484225074!5m2!1sen!2sin\" width=\"400\" height=\"450\" style=\"border:0;\" allowfullscreen=\"\" loading=\"lazy\"></iframe>', 'Mon-Sat: 11:00 AM - 23:00 PM', '201/1, Demo, Bahirhat , Barasat, 700011', 'admin@admin.com', '8981224073', '8867764866453', '8981224073', 'Hi we are wadding era ');

-- --------------------------------------------------------

--
-- Table structure for table `header_footer`
--

CREATE TABLE `header_footer` (
  `id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `header_logo` varchar(255) NOT NULL,
  `header_logo_uri` varchar(255) NOT NULL,
  `header_contact` varchar(255) NOT NULL,
  `header_time` varchar(255) NOT NULL,
  `footer_logo` varchar(255) NOT NULL,
  `footer_logo_uri` varchar(255) NOT NULL,
  `footer_content` text NOT NULL,
  `footer_facebook` varchar(255) NOT NULL,
  `footer_insta` varchar(255) NOT NULL,
  `footer_twetter` varchar(255) NOT NULL,
  `footer_linkedin` varchar(255) NOT NULL,
  `footer_youtube` varchar(255) NOT NULL,
  `footer_coppyright` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `header_footer`
--

INSERT INTO `header_footer` (`id`, `header_logo`, `header_logo_uri`, `header_contact`, `header_time`, `footer_logo`, `footer_logo_uri`, `footer_content`, `footer_facebook`, `footer_insta`, `footer_twetter`, `footer_linkedin`, `footer_youtube`, `footer_coppyright`) VALUES
('42344255', '1640080415402logo_png.png', 'http://localhost:4301/uploads\\1640080415402logo_png.png', '+91 8981-22-4073', '11:00 AM - 23:00 PM', '1640080425435logo_png.png', 'http://localhost:4301/uploads\\1640080425435logo_png.png', 'Et aut eum quis fuga eos sunt ipsa nihil. Labore corporis magni eligendi fuga maxime saepe commodi placeat.', 'https://www.facebook.com/', 'https://www.instagram.com/', 'https://twitter.com/', 'https://www.linkedin.com/signup', 'http://youtube.com/', '© Copyright Wedding-Era. All Rights Reserved');

-- --------------------------------------------------------

--
-- Table structure for table `home_banner`
--

CREATE TABLE `home_banner` (
  `id` varchar(255) NOT NULL,
  `bannerImageName` varchar(255) NOT NULL,
  `bannerImageUrl` varchar(255) NOT NULL,
  `bannerHeading` text NOT NULL,
  `bannerSubHeading` text NOT NULL,
  `bannerImageBase` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `home_banner`
--

INSERT INTO `home_banner` (`id`, `bannerImageName`, `bannerImageUrl`, `bannerHeading`, `bannerSubHeading`, `bannerImageBase`) VALUES
('73388133', '164008003234514.jpg', 'http://localhost:4301/uploads\\164008003234514.jpg', 'wedding era', 'Ut velit est quam dolor ad a aliquid qui aliquid. ', 'uploads\\164008003234514.jpg'),
('75718711', '16400800483477.jpg', 'http://localhost:4301/uploads\\16400800483477.jpg', 'wedding era', 'Ut velit est quam dolor ad a aliquid qui aliquid.', 'uploads\\16400800483477.jpg'),
('06356561', '164008007740410.jpg', 'http://localhost:4301/uploads\\164008007740410.jpg', 'wedding era', 'Ut velit est quam dolor ad a aliquid qui aliquid.', 'uploads\\164008007740410.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `home_widget`
--

CREATE TABLE `home_widget` (
  `id` varchar(255) NOT NULL,
  `widgetNumber` varchar(255) NOT NULL,
  `widgetHeading` varchar(255) NOT NULL,
  `widgetContent` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `home_widget`
--

INSERT INTO `home_widget` (`id`, `widgetNumber`, `widgetHeading`, `widgetContent`) VALUES
('72167871', '01', 'Lorem Ipsum', 'Ulamco laboris nisi ut aliquip ex ea commodo consequat. Et consectetur ducimus vero placeat\n\n');

-- --------------------------------------------------------

--
-- Table structure for table `image_gallery`
--

CREATE TABLE `image_gallery` (
  `galleryName` varchar(255) NOT NULL,
  `galleryId` varchar(255) NOT NULL,
  `imageName` varchar(255) NOT NULL,
  `imageUrl` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `imageBasePath` varchar(255) NOT NULL,
  `imageId` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `image_gallery`
--

INSERT INTO `image_gallery` (`galleryName`, `galleryId`, `imageName`, `imageUrl`, `imageBasePath`, `imageId`) VALUES
('birthday', '83216250', '1639997769354gallery-3.jpg', 'http://localhost:4301/uploads\\1639997769354gallery-3.jpg', 'uploads\\1639997769354gallery-3.jpg', '70445643'),
('birthday', '83216250', '1639997769358gallery-4.jpg', 'http://localhost:4301/uploads\\1639997769358gallery-4.jpg', 'uploads\\1639997769358gallery-4.jpg', '52174658'),
('birthday', '83216250', '1639997769362gallery-5.jpg', 'http://localhost:4301/uploads\\1639997769362gallery-5.jpg', 'uploads\\1639997769362gallery-5.jpg', '03117613'),
('birthday', '83216250', '1639997769678gallery-6.jpg', 'http://localhost:4301/uploads\\1639997769678gallery-6.jpg', 'uploads\\1639997769678gallery-6.jpg', '56476671'),
('Pre Weeding', '41796877', '1639998096295gallery-4.jpg', 'http://localhost:4301/uploads\\1639998096295gallery-4.jpg', 'uploads\\1639998096295gallery-4.jpg', '18606739'),
('Pre Weeding', '41796877', '1639998096301gallery-5.jpg', 'http://localhost:4301/uploads\\1639998096301gallery-5.jpg', 'uploads\\1639998096301gallery-5.jpg', '58181326'),
('Pre Weeding', '41796877', '1639998096306gallery-6.jpg', 'http://localhost:4301/uploads\\1639998096306gallery-6.jpg', 'uploads\\1639998096306gallery-6.jpg', '81770864'),
('birthday', '83216250', '1639998108307gallery-6.jpg', 'http://localhost:4301/uploads\\1639998108307gallery-6.jpg', 'uploads\\1639998108307gallery-6.jpg', '27596182'),
('birthday', '83216250', '1639998108309gallery-7.jpg', 'http://localhost:4301/uploads\\1639998108309gallery-7.jpg', 'uploads\\1639998108309gallery-7.jpg', '73548481'),
('birthday', '83216250', '1639998108313gallery-8.jpg', 'http://localhost:4301/uploads\\1639998108313gallery-8.jpg', 'uploads\\1639998108313gallery-8.jpg', '76868561'),
('Baby Photo', '09325346', '1640027115712testimonials-2.jpg', 'http://localhost:4301/uploads\\1640027115712testimonials-2.jpg', 'uploads\\1640027115712testimonials-2.jpg', '81186264'),
('Baby Photo', '09325346', '1640027115719testimonials-3.jpg', 'http://localhost:4301/uploads\\1640027115719testimonials-3.jpg', 'uploads\\1640027115719testimonials-3.jpg', '47818667'),
('Baby Photo', '09325346', '1640027115728testimonials-4.jpg', 'http://localhost:4301/uploads\\1640027115728testimonials-4.jpg', 'uploads\\1640027115728testimonials-4.jpg', '34536319'),
('Baby Photo', '09325346', '1640027115732testimonials-5.jpg', 'http://localhost:4301/uploads\\1640027115732testimonials-5.jpg', 'uploads\\1640027115732testimonials-5.jpg', '52674647'),
('asdasda', '67876731', '1640027161898testimonials-1.jpg', 'http://localhost:4301/uploads\\1640027161898testimonials-1.jpg', 'uploads\\1640027161898testimonials-1.jpg', '37662428'),
('asdasda', '67876731', '1640027161906testimonials-2.jpg', 'http://localhost:4301/uploads\\1640027161906testimonials-2.jpg', 'uploads\\1640027161906testimonials-2.jpg', '25216587'),
('asdasda', '67876731', '1640027161914testimonials-3.jpg', 'http://localhost:4301/uploads\\1640027161914testimonials-3.jpg', 'uploads\\1640027161914testimonials-3.jpg', '54159068'),
('asdasda', '67876731', '1640027161918testimonials-4.jpg', 'http://localhost:4301/uploads\\1640027161918testimonials-4.jpg', 'uploads\\1640027161918testimonials-4.jpg', '02325796');

-- --------------------------------------------------------

--
-- Table structure for table `member_details`
--

CREATE TABLE `member_details` (
  `id` varchar(255) NOT NULL,
  `memberImageName` varchar(255) NOT NULL,
  `memberImageUrl` varchar(255) NOT NULL,
  `memberImageBase` varchar(255) NOT NULL,
  `memberName` text NOT NULL,
  `memberPost` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `member_details`
--

INSERT INTO `member_details` (`id`, `memberImageName`, `memberImageUrl`, `memberImageBase`, `memberName`, `memberPost`) VALUES
('51637454', '1639480285744chefs-1.jpg', 'http://localhost:4301/uploads\\1639480285744chefs-1.jpg', 'uploads\\1639480285744chefs-1.jpg', 'Walter White', 'Master Chef'),
('61143480', '1639480300878chefs-2.jpg', 'http://localhost:4301/uploads\\1639480300878chefs-2.jpg', 'uploads\\1639480300878chefs-2.jpg', 'Sarah Jhonson', 'Patissier'),
('28507482', '1639480316727chefs-3.jpg', 'http://localhost:4301/uploads\\1639480316727chefs-3.jpg', 'uploads\\1639480316727chefs-3.jpg', 'William Anderson', 'Cook');

-- --------------------------------------------------------

--
-- Table structure for table `other_content`
--

CREATE TABLE `other_content` (
  `id` varchar(2400) NOT NULL,
  `whyChooseUsHeading` text NOT NULL,
  `whyChooseUsContent` text NOT NULL,
  `serviceHeading` text NOT NULL,
  `serviceContent` text NOT NULL,
  `eventContent` text NOT NULL,
  `eventHeading` text NOT NULL,
  `galleryContent` text NOT NULL,
  `galleryHeading` text NOT NULL,
  `teamHeading` text NOT NULL,
  `teamContent` text NOT NULL,
  `contactHeading` text NOT NULL,
  `contactContent` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `other_content`
--

INSERT INTO `other_content` (`id`, `whyChooseUsHeading`, `whyChooseUsContent`, `serviceHeading`, `serviceContent`, `eventContent`, `eventHeading`, `galleryContent`, `galleryHeading`, `teamHeading`, `teamContent`, `contactHeading`, `contactContent`) VALUES
('72484492', 'Why choose Our Restaurant', 'Ut possimus qui ut temporibus culpa velit eveniet modi omnis est adipisci expedita at voluptas atque vitae autem.\n\n', 'Check our Specials', 'Ut possimus qui ut temporibus culpa velit eveniet modi omnis est adipisci expedita at voluptas atque vitae autem.\n\n', 'Ut possimus qui ut temporibus culpa velit eveniet modi omnis est adipisci expedita at voluptas atque vitae autem.\n\n', 'Organize Your Events With Us', 'Ut possimus qui ut temporibus culpa velit eveniet modi omnis est adipisci expedita at voluptas atque vitae autem.\n\n', 'Some photos from Our Work', 'Our Proffesional Team', 'Ut possimus qui ut temporibus culpa velit eveniet modi omnis est adipisci expedita at voluptas atque vitae autem.\n\n', 'Contact Us', 'Ut possimus qui ut temporibus culpa velit eveniet modi omnis est adipisci expedita at voluptas atque vitae autem.\n\n');

-- --------------------------------------------------------

--
-- Table structure for table `service_page`
--

CREATE TABLE `service_page` (
  `id` varchar(255) NOT NULL,
  `serviceHeading` text NOT NULL,
  `serviceContent` text NOT NULL,
  `serviceName` text NOT NULL,
  `serviceImageName` varchar(255) NOT NULL,
  `serviceImageUrl` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `service_page`
--

INSERT INTO `service_page` (`id`, `serviceHeading`, `serviceContent`, `serviceName`, `serviceImageName`, `serviceImageUrl`) VALUES
('41574486', 'Architecto ut aperiam autem id', '<p class=\"fst-italic\"><em>Qui laudantium consequatur laborum sit qui ad sapiente dila parde sonata raqer a videna mareta paulona marka</em></p>\n<p><em>Et nobis maiores eius. Voluptatibus ut enim blanditiis atque harum sint. Laborum eos ipsum ipsa odit magni. Incidunt hic ut molestiae aut qui. Est repellat minima eveniet eius et quis magni nihil. Consequatur dolorem quaerat quos qui similique accusamus nostrum rem vero</em></p>', 'Modi sit est', '164008023464921.jpg', 'http://localhost:4301/uploads\\164008023464921.jpg'),
('24316940', 'Et blanditiis nemo veritatis excepturi', '<p class=\"fst-italic\">Qui laudantium consequatur laborum sit qui ad sapiente dila parde sonata raqer a videna mareta paulona marka</p>\n<p>Ea ipsum voluptatem consequatur quis est. Illum error ullam omnis quia et reiciendis sunt sunt est. Non aliquid repellendus itaque accusamus eius et velit ipsa voluptates. Optio nesciunt eaque beatae accusamus lerode pakto madirna desera vafle de nideran pal</p>', 'Unde praesentium sed', '164008021555112.jpg', 'http://localhost:4301/uploads\\164008021555112.jpg'),
('83543164', 'Et blanditiis nemo veritatis excepturi', '<p class=\"fst-italic\">Qui laudantium consequatur laborum sit qui ad sapiente dila parde sonata raqer a videna mareta paulona marka</p>\n<p>Ea ipsum voluptatem consequatur quis est. Illum error ullam omnis quia et reiciendis sunt sunt est. Non aliquid repellendus itaque accusamus eius et velit ipsa voluptates. Optio nesciunt eaque beatae accusamus lerode pakto madirna desera vafle de nideran pal</p>', 'Pre Wedding ', '16400801994989.jpg', 'http://localhost:4301/uploads\\16400801994989.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `testimonial_page`
--

CREATE TABLE `testimonial_page` (
  `id` varchar(255) NOT NULL,
  `testimonialImageName` text NOT NULL,
  `testimonialImageUrl` varchar(255) NOT NULL,
  `testimonialMessage` text NOT NULL,
  `testimonialClientName` varchar(255) NOT NULL,
  `testimonialClientPost` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `testimonial_page`
--

INSERT INTO `testimonial_page` (`id`, `testimonialImageName`, `testimonialImageUrl`, `testimonialMessage`, `testimonialClientName`, `testimonialClientPost`) VALUES
('41404773', '1639575875831testimonials-1.jpg', 'http://localhost:4301/uploads\\1639575875831testimonials-1.jpg', ' Proin iaculis purus consequat sem cure digni ssim donec porttitora entum suscipit rhoncus. Accusantium quam, ultricies eget id, aliquam eget nibh et. Maecen aliquam, risus at semper. ', 'Saul Goodman', 'Ceo & Founder'),
('75624261', '1640024972177testimonials-5.jpg', 'http://localhost:4301/uploads\\1640024972177testimonials-5.jpg', 'Proin iaculis purus consequat sem cure digni ssim donec porttitora entum suscipit rhoncus. Accusantium quam, ultricies eget id, aliquam eget nibh et. Maecen aliquam, risus at semper.	', 'Abhimoy Kolay', 'CEO Weeding era');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
